<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(config('tables.user'), function (Blueprint $table) {
			$table->bigIncrements('user_id');
			$table->string('user_name');
			$table->boolean('user_status');
			$table->integer('user_cedula');
			$table->string('user_rol');
			$table->string('user_email')->unique();
			$table->string('user_password');
			$table->text('user_token_password');
			$table->dateTime('user_token_before'); // Cuanto tiempo estara displonible el cambio de contrasena
			$table->boolean('user_token_checked'); // Esta habilitado el cambio de contrasena
			$table->timestamp('user_created')->nullable();
			$table->timestamp('user_updated')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(config('tables.user'));
	}
}
