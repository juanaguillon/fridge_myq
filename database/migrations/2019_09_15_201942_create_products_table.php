<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('tables.products'), function (Blueprint $table) {
            $table->bigIncrements('prod_id');
            $table->string('prod_nombre',200);
            $table->integer('prod_price');
            $table->integer('prod_off')->nullable();
            $table->integer('prod_status');
            $table->string('prod_image',245);
            $table->text('prod_desc')->nullable();
            $table->timestamp('prod_created')->nullable();
            $table->timestamp('prod_updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('tables.products'));
    }
}
