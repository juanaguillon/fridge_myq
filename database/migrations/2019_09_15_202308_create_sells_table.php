<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(config('tables.sells'), function (Blueprint $table) {
			$table->bigIncrements('sell_id');
			$table->integer('sell_user');
			$table->integer('sell_prod');
			$table->integer('sell_price');
			$table->integer('sell_quantity');
			$table->boolean('sell_status');
			$table->timestamp('sell_created')->nullable();
			$table->timestamp('sell_updated')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists(config('tables.sells'));
	}
}
