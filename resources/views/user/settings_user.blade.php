@extends('layouts.section')
@section('title','Mi Configuración')
@section('section_id', 'all_users_section')
@section('subcontent')

<div class="box">
  <div class="has-text-centered">
    <h3 class="title is-4 has-text-link">Tu configuración</h3>
  </div>
  <form method="POST" action={{url('user/settings')}}>
    @csrf
    @if (isset($error))
    <div class="message is-danger">
      <div class="message-body">
        {{$error}}
      </div>
    </div>
    @elseif ( isset($success))
    <div class="message is-success">
      <div class="message-body">
        {{$success}}
      </div>
    </div>
    @endif
    <div class="columns is-multiline">
      <div class="column is-6-tablet is-12-mobile">
        <div class="field">
          <label class='label'>Email</label>
          <input type="email" value="{{$current_user->user_email}}" disabled class="input">
        </div>
      </div>
      <div class="column is-6-tablet is-12-mobile"></div>
      <div class="column is-6-tablet is-12-mobile">
        <div class="field">
          <label class='label' for="settings_cedula">Cédula</label>
          <input value="{{$current_user->user_cedula}}" type="number" id='settings_cedula' name='settings_cedula'
            class="input">
        </div>
      </div>
      <div class="column is-6-tablet is-12-mobile">
        <div class="field">
          <label class='label' for="settings_name">Nombre y apellido</label>
          <input type="text" value="{{$current_user->user_name}}" id='settings_name' name='settings_name' class="input">
        </div>
      </div>
      <div class="column is-6-tablet is-12-mobile">
        <div class="field">
          <div class="custom_checkbox" style="margin:0 4px">
            <input id='change_password_checker' name='settings_password' type="checkbox">
            <i class="icon-checkmark"></i>
          </div>
          <label for='change_password_checker' class='checkbox'>
            ¿Cambiar contraseña?
          </label>
          
        </div>
      </div>
      <div class="column is-6-tablet is-12-mobile"></div>

      <div class="column is-6-tablet is-12-mobile need_password_checker hidden">
        <div class="field">
          <label class='label' for="settings_cpassword">Contraseña Actual</label>
          <input type="password" id='settings_cpassword' name='settings_cpassword' class="input">
        </div>
      </div>
      <div class="column is-6-tablet is-12-mobile need_password_checker hidden">
        <div class="field">
          <label class='label' for="settings_newpassword">Nueva Contraseña</label>
          <input type="password" id='settings_newpassword' name='settings_newpassword' class="input">
        </div>
      </div>

    </div>
    <div class="field has-text-centered">
      <button class="button is-success">Guardar cambios</button>
    </div>

  </form>
</div>

@endsection