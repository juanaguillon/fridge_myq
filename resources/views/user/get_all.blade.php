@extends('layouts.section')
@section('title','Usuarios Activos')
@section('section_id', 'all_users_section')
@section('subcontent')
    
  @if ( $all_users )
    <div class="columns is-multiline">
      <input type="hidden" value='{{csrf_token()}}' id="_token_id">
      @foreach ($all_users as $user)
        <div class="column is-8-tablet">
          <div class="box">
            <h5 class="subtitle is-5 has-text-link is-marginless">{{ $user->user_name }}</h5>
            <span class="subtitle is-5 has-text-dark-grey">{{ $user->user_email }}</span>
          </div>
        </div>
        <div class="column is-4">
          <div class="box detail_total_box">
            @if ( $user->user_status == 1 )
            <button data-user_toggle='{{$user->user_id}}' class="button_toggle_user button is-danger has-text-weight-bold">Deshabilitar</button>
            @else
            <button data-user_toggle='{{$user->user_id}}' class="button_toggle_user button is-success has-text-weight-bold">Habilitar</button>
            @endif
          </div>
        </div>
      @endforeach
    </div>

  @endif

@endsection
