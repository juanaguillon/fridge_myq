@extends("layouts.section")
@section('title','Datos actuales')
@section("section_id", 'user_initial_section')
@section("subcontent")
<h3 class="title is-3">Detalles actuales</h3>
<div class="user_details_abs_wrap">
  <div class="user_details_name">
    <span class="user_name">{{ session()->get('user_name') }}</span>
    <span class="user_email">{{ session()->get('user_email') }}</span>
  </div>
  <div class="columns">
    <div class="column is-9">
      <div class="user_details_products">

      </div>
    </div>
    <div class="columns is-3">
      <div class="user_details_doubt">

      </div>
    </div>
  </div>


</div>
</div>

</section>

@endsection