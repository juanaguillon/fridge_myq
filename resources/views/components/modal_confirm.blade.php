<div id="modal_confirm" class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">{{$title}}</p>
      <button class="delete" aria-label="close"></button>
    </header>
    <section class="modal-card-body">
      <p>{{$text}}</p>
    </section>
    <footer class="modal-card-foot">
      <button id="confirm_modal_confirm" class="button is-success">{{$continue}}</button>
    <button id="close_modal_confirm" class="button is-danger">{{$cancel}}</button>
    </footer>
  </div>
</div>

<div id="modal_message" class="modal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <div class="box has-text-centered">
      <p id='modal_message_text'></p>
      <button id="close_modal_message" class="button is-dark" aria-label="close">Entendido</button>
    </div>
  </div>
</div>