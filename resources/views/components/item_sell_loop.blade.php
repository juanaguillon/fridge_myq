{{-- Es igual a item_sell, a diferencia que este es un archivo que se puede pasar el array de las ventas, y este creara el loop correspondiente. --}}
@foreach ($sells as $sell)
<div class="column is-12-mobile is-3-widescreen is-4-desktop is-6-tablet">
  <div class="sell_item ">
    <div class="sell_item_wrap">
      @if ($sell->sell_status == 0)
      <div class="sell_tag">
        <span class="tag is-success">Saldado</span>
      </div>
      @endif
      <div class="card">
        <div class="card-image">
          <figure class="sell_figure_item image is-64x64">
            <img src="{{asset('storage') . '/' . $sell->prod_image }}" alt="{{$sell->prod_nombre}}">
          </figure>
        </div>
        <div class="sell_card_content_item card-content">
          <div class="sell_media_item media">

            <div class="media-content">
              <p class="sell_title_item subtitle is-6 has-text-weight-bold">{{$sell->prod_nombre}}
                <span class='has-text-link'> X{{$sell->sell_quantity}}</span>
              </p>
              <p class="subtitle is-7 has-text-weight-bold">{{ formatMoney($sell->sell_price) }}</p>
            </div>
          </div>
          @if (session()->get('user_rol') == 'admin')
          <div class="content">
            <a href="{{url('sell/edit-sell/' . $sell->sell_id)}}" class="button is-dark">Editar</a>
            @endif
            @if ($sald ?? false)
            <button data-token="{{csrf_token()}}" data-sald='{{$sell->sell_id}}'
              class="button is-success sell_sald_button">Saldar</button>
            @endif
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endforeach