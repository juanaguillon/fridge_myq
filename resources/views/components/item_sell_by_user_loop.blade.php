@if ( count($allusers) > 0 )
@foreach ($allusers as $user)
<div id='details_abs_{{$user['user_id']}}' class="details_abs_wrap">
  <div class="details_wrap">
    <div class="columns is-multiline">
      <div class="sell_user_data column is-4">
        <div class="box detail_total_box">
          <p class='subtitle is-6 has-text-white is-marginless'>{{$user['user_email']}}</p>
          <span class='subtitle is-6 has-text-weight-bold has-text-white'>{{formatUserName($user['user_name'])}}</span>
        </div>
      </div>
      <div class="column is-12 is-10-desktop">
        <div class="sell_container">
          <div class="columns is-vcentered sell_container_clms">
            @component('components.item_sell_loop', array(
            'sells' => $user['user_sells'],
            'sald' => $sald ?? false
            ))
            @endcomponent
          </div>
        </div>
      </div>
      <div class="column is-12 is-2-desktop">
        <div class="box detail_total_box has-text-centered">
          <p class='subtitle is-5 has-text-white'>Total</p>
          <span class='subtitle is-4 has-text-white sell_user_total'>{{formatMoney($user['user_total'])}}</span>
          @if ($sald ?? false)
          <button data-token={{ csrf_token() }} data-user_sald="{{$user['user_id']}}"
            class="button is-info sald_all_sells_user">Saldar</button>
          @endif
        </div>
      </div>
    </div>

  </div>
</div>

@endforeach

@else
<h3 class='title is-4 has-text-link' style='margin-bottom:50px'>No se han encontrado ventas.</h3>
@endif