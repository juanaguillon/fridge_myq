<div class="sell_item">
  <div class="sell_item_wrap">
    @if ($sell_status == 0)
    <div class="sell_tag">
      <span class="tag is-success">Saldado</span>
    </div>

    @endif
    <div class="card">
      <div class="card-image">
        <figure class="sell_figure_item image is-64x64">
          <img src="{{asset('storage') . '/' . $sell_image }}" alt="{{$sell_title}}">
        </figure>
      </div>
      <div class="sell_card_content_item card-content">
        <div class="sell_media_item media">

          <div class="media-content">
            <p class="sell_title_item subtitle is-6 has-text-weight-bold">{{$sell_title}}
              <span class='has-text-link'> X{{$sell_quantity}}</span>
            </p>
            <p class="subtitle is-7 has-text-weight-bold">{{ formatMoney($sell_price) }}</p>
          </div>
        </div>

        @if ( session()->get('user_rol') == 'admin' && $show_button_sald )
        <div class="content">
          <button data-token="{{csrf_token()}}" data-sald='{{$sell_id}}'
            class="button is-success sell_sald_button">Saldar</button>
          <a href="{{url('sell/edit-sell/' . $sell_id)}}" class="button is-dark">Editar</a>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>