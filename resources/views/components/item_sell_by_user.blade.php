<div id='details_abs_{{$user_id}}' class="details_abs_wrap">
  <div class="details_wrap">
    <div class="columns is-multiline">
      <div class="sell_user_data column is-12-mobile is-4-desktop is-6-tablet">
        <div class="box detail_total_box">
          <p class='subtitle is-6 has-text-white is-marginless'>{{$user_email}}</p>
          <span class='subtitle is-6 has-text-weight-bold has-text-white'>{{ formatUserName($user_name)}}</span>
        </div>
      </div>
      <div class="column is-12 is-10-desktop">
        <div class="sell_container">
          <div class="columns is-vcentered sell_container_clms">
            {{$slot}}
          </div>
        </div>
      </div>
      <div class="column is-12 is-2-desktop">
        <div class="box detail_total_box has-text-centered">
          <p class='subtitle is-5 has-text-white'>Total</p>
          <span class='subtitle is-4 has-text-white sell_user_total'>{{ formatMoney( $user_total)}}</span>
          @if ( session()->get('user_rol') == 'admin' && $show_button_sald )

          <button data-token={{ csrf_token() }} data-user_sald="{{$user_id}}" class="button is-info sald_all_sells_user">Saldar</button>
          
          @endif
        </div>
      </div>
    </div>

  </div>
</div>