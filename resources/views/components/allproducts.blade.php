@if ( count( $products) > 0 )
@php
$urlprod = URL::to('/public') . '/storage/';
@endphp
@foreach ($products as $product)

<input type="hidden" value="{{ $product->prod_status }}">
<div class="column is-3-desktop is-6-tablet is-12-mobile">
  <div class="card product_card">
    <div class="card-image">
      <figure class="image is-square">
        <img src='{{ $urlprod . $product->prod_image }}' alt="{{ $product->prod_nombre }}" />
      </figure>

    </div>
    <div class="card-content">
      <div class="media">
        <div class="media-content">
          <p class="title is-5 has-text-link">{{ $product->prod_nombre }}</p>
        </div>
      </div>

      <div class="content">
        @php
        $percent = ($product->prod_price * $product->prod_off) / 100;
        $disCount = $product->prod_price - $percent;
        @endphp
        @if ( $product->prod_off )
        <span class="has-text-grey text-lined subtitle is-6">${{number_format($product->prod_price,0,'.','.') }}</span>
        <h5 class="subtitle is-6">${{ number_format($disCount,0,'.','.')  }}</h5>
        @else
        <h5 class="subtitle is-6">${{ number_format($disCount,0,'.','.')  }}</h5>
        @endif
        <div class="product_action_card">
          <div class="field has-addons">
            <div class="control w-100">
              <input value="1" min='1' max='15' class="input" type="number">
            </div>
            <div class="control">
              <button data-price="{{$disCount}}" data-prod='{{$product->prod_id}}'
                class="add_sell_button button is-success">
                &#x2b; Agregar
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" value="{{ csrf_token() }}" id="csrf_token">

@endforeach
@else
<h3 class='title is-4 has-text-link'>No se ha encotrado productos.</h3>

@endif