@extends("layouts.section")
@section('title', 'Productos')

@section("subcontent")
@if ( count($products) > 0 )
<div id="search_by_user">
  <div class="field search_by_user_wrap">
    <h3 class="title is-4 has-text-link">
      Buscar Productos
    </h3>
    <article id="no_search_passed" class="hidden message is-danger">
      <div class="message-body">
        Debe ingresar un nombre de producto para buscar.
      </div>
    </article>
    <input placeholder="Nombre de producto" type="text" id="search_by_product_input" class="input">
  </div>
  <button id="search_by_product_button" data-token="{{csrf_token()}}"
    class="button is-link">Buscar</button>
  <button id='search_all_products' data-token="{{csrf_token()}}" class='button is-gray'>Ver todo</button>
</div>

<div id='all_products_container' class="columns is-multiline">
  @component('components.allproducts', ['products' => $products])
  @endcomponent
  
</div>
@component('components.modal_confirm', array(
"title" => 'Anadir producto',
"text" => "Está seguro/a de agregar el producto a su historial?",
"continue" => "Agregar",
"cancel" => "Cancelar"
))
@endcomponent

@else
<h4 class="title is-4 has-text-link">No existen prouctos actualmente.</h4>
@endif
@endsection