@extends("layouts.section")
@section('title','Editar Producto')

@section('section_id','create_product_section')
@section("subcontent")

<h3 class="title is-3 has-text-link">Editar Producto</h3>
<div class="create_product_container">
  <div class="create_product_box">
    <div class="box">
      <article id="product_message_error" class="notification is-danger hidden">
        <p id="message_error_data"></p>
      </article>

      @if ( isset($_GET['newprod']))
      @if ( $_GET['newprod'] === 'success')
      <div class="notification is-success">
        Se ha modificado el producto correctamente.
      </div>
      @else
      <div class="notification is-danger">
        Error al editar el producto, intente nuevamente.
      </div>
      @endif
      @endif
      <div class="create_product_form_wrap">
        <form enctype="multipart/form-data" action="{{url('product/edit')}}" method="POST" id="create_product_form">
          @csrf
          <div class="current_image_prod">
            <figure class='image is-128x128'>
              <img id='view_image_prod' src="{{asset('storage/' . $product->prod_image)}}" alt="">
            </figure>
          </div>
          <div class="columns is-multiline">
            <div class="column is-6">
              <div class="field">
                <div class="control">
                  <label class='label' for="create_product_title">Nombre</label>
                  <input name='create_product_title' value='{{$product->prod_nombre}}' placeholder="Ingrese el nombre del producto" type="text" class="input" id="create_product_title">
                </div>
              </div>
            </div>
            <div class="column is-6">
              <div class="field">
                
                <label class="label">Imagen</label>
                <div class="file has-name">
                  <label class="file-label">
                    <input id="create_product_image" name='create_product_image' class="omit_verify file-input" type="file" >
                    <span class="file-cta">
                      <span class="file-icon">
                        <i class="icon-upload"></i>
                      </span>
                      <span class="file-label">
                        Seleccione Imagen
                      </span>
                    </span>
                    
                    <span id='img_name' class="file-name">
                      {{$product->prod_image}}
                    </span>
                  </label>
                </div>
              </div>
            </div>

            <div class="column is-4">
              <label for="create_product_price" class="label">Precio</label>

              <div class="field has-addons">
                <span class="control">
                  <button class="button is-static">$</button>
                </span>
                <div class="control w-100">
                  <input name="create_product_price" placeholder="Ingrese el precio del producto" type="number" class="input " id="create_product_price" value='{{ $product->prod_price }}'>
                </div>

              </div>

            </div>
            <div class="column is-4">
              <label for="create_product_off" class="label">Descuento ( Opcional )</label>

              <div class="field has-addons">
                <div class="control w-100">
                  <input value='{{$product->prod_off}}' placeholder="Ingrese el descuento del producto" type="number" name='create_product_off' class="input " id="create_product_off">
                </div>
                <span class="control">
                  <button class="button is-static">%</button>
                </span>
              </div>
            </div>
            <div class="column is-4">
              <div class="field is-grouped" style="flex-direction:column">
                <label for="edit_sell_product" class="label">¿Está activo?</label>
                <div class="custom_checkbox" style="margin:0 8px">
                  @if ($product->prod_status == "1")
                  <input checked type="checkbox" name="new_status_prod" id="edit_active_product">
                  @else
                  <input type="checkbox" name="new_status_prod" id="edit_active_product">
                  @endif
                  <i class="icon-checkmark"></i>
                </div>
                <small>Verdadero = Esta activo actualmente.</small>
              </div>
            </div>
          </div> <!-- Final Columns -->
          <label for="create_product_description" class="label">Descripcion ( Opcional )</label>
          <div class="field">

            <div class="control">
              <textarea placeholder="Ingrese la descripcion del producto" name="create_product_description" id="create_product_description" class="textarea">{{$product->prod_desc}}</textarea>
            </div>
          </div>
          <div class="field">
            <div class="control">
              <input type="hidden" name="create_product_id" id='create_product_id' value={{$product->prod_id}}>
              <button class="button is-link">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

@endsection