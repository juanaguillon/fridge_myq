@extends("layouts.section")
@section('title','Crear Producto')

@section('section_id','create_product_section')
@section("subcontent")

<h3 class="title is-3 has-text-link">Crear Producto</h3>
<div class="create_product_container">
  <div class="create_product_box">
    <div class="box">
      <article id="product_message_error" class="notification is-danger hidden">
        <p id="message_error_data"></p>
      </article>

      @if ( isset($_GET['newprod']))
      @if ( $_GET['newprod'] === 'success')
      <div class="notification is-success">
        Se ha creado el producto correctamente.
      </div>
      @else
      <div class="notification is-danger">
        Error al crear el producto, intente nuevamente.
      </div>
      @endif
      @endif
      <div class="create_product_form_wrap">
        <form enctype="multipart/form-data" action="{{url('product/create')}}" method="POST" id="create_product_form">
          @csrf
          <div class="current_image_prod">
            <figure class='image is-128x128'>
              <img id='view_image_prod' src="" alt="">
            </figure>
          </div>
          <div class="columns is-multiline">
            <div class="column is-6">
              <div class="field">
                <div class="control">
                  <label class='label' for="create_product_title">Nombre</label>
                  <input name='create_product_title' placeholder="Ingrese el nombre del producto" type="text" class="input" id="create_product_title">
                </div>
              </div>
            </div>
            <div class="column is-6">
              <div class="field">
                <label class="label">Imagen</label>
                <div class="file has-name">
                  <label class="file-label">
                    <input id="create_product_image" name='create_product_image' class="file-input" type="file" name="resume">
                    <span class="file-cta">
                      <span class="file-icon">
                        <i class="icon-upload"></i>
                      </span>
                      <span class="file-label">
                        Seleccione Imagen
                      </span>
                    </span>
                    <span id='img_name' class="file-name">
                      Vacio
                    </span>
                  </label>
                </div>
              </div>
            </div>

            <div class="column is-6">
              <label for="create_product_price" class="label">Precio</label>

              <div class="field has-addons">
                <span class="control">
                  <button class="button is-static">$</button>
                </span>
                <div class="control w-100">
                  <input name="create_product_price" placeholder="Ingrese el precio del producto" type="number" class="input " id="create_product_price">
                </div>

              </div>

            </div>
            <div class="column is-6">
              <label for="create_product_off" class="label">Descuento ( Opcional )</label>

              <div class="field has-addons">
                <div class="control w-100">
                  <input placeholder="Ingrese el descuento del producto" type="number" name='create_product_off' class="input " id="create_product_off">
                </div>
                <span class="control">
                  <button class="button is-static">%</button>
                </span>
              </div>
            </div>
          </div> <!-- Final Columns -->
          <label for="create_product_description" class="label">Descripcion ( Opcional )</label>
          <div class="field">

            <div class="control">
              <textarea placeholder="Ingrese la descripcion del producto" name="create_product_description" id="create_product_description" class="textarea"></textarea>
            </div>
          </div>
          <div class="field">
            <div class="control">
              <button class="button is-link">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

@endsection