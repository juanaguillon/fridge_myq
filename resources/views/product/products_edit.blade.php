@extends("layouts.section")
@section('title', 'Editar productos')

@section("subcontent")
@if ( count($products) > 0 )
<div class="columns is-multiline">
  @foreach( $products as $product )
  <div class="column is-one-quarter">
    <div class="card product_card">
      <div class="card-image">
        <figure class="image is-128x128 m-auto">
          <img src='{{ $urlprod . $product->prod_image }}' alt="{{ $product->prod_nombre }}" />
        </figure>
      </div>
      <div class="card-content">
        <div class="media">
          <div class="media-content">
            <p class="title is-6 has-text-link">{{ $product->prod_nombre }}</p>
          </div>
        </div>

        <div class="content">
          @if ( ! $product->prod_off )
            <span class="has-text-grey subtitle is-6">0%</span>
          @else
            <span class="has-text-grey subtitle is-6">{{$product->prod_off}}%</span>
          @endif


          @if ($product->prod_status == "1")
          <span class="has-text-success subtitle is-6">Activo</span>
          @else
          <span class="has-text-danger subtitle is-6">Desactivado</span>
          @endif
          
          <h5 class="subtitle is-6">${{ number_format($product->prod_price,0,'.','.')  }}</h5>
         
          <div class="product_action_card">
              <div class="control">
                <a target="_blank" href={{url('product/edit') . '/' . $product->prod_id }} class="button is-info">
                  <span class="icon">
                    <i class="icon-pencil"></i> 
                  </span>
                  <span>Editar</span>
                </a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

@else
<h4 class="title is-4 has-text-link">No existen prouctos actualmente.</h4>
@endif
@endsection