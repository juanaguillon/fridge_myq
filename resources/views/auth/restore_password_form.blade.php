@extends("layouts.app")

@section("title","Restablecer Contraseña")
@section("content")

<section class="section" id="restore_pass_section">
  <div class="register_abs_wrap">
    <div class="register_box">
      <div class="box">

        <h4 class="title is-4 has-text-link">Restablecer Contraseña</h4>
        <div class="register_form_wrap">

          <article id='restore_pwtoken_message' class="message is-danger hidden">
            <div class="message-body">
              Las contraseñas no coinciden.
            </div>
          </article>

          @if(isset($error))
          <article class="message is-danger">
            <div class="message-body">
              {{$error}}
            </div>
          </article>
          @elseif(isset($success))
          <article class="message is-success">
            <div class="message-body">
              {{$success}}
            </div>
          </article>
          @endif

          <form action="{{url('auth/restore-password-tokenized')}}" method="POST" id="restore_passtoken_form">
            @csrf
            <input type="hidden" value='{{$tokenize}}' name="restore_pwtoken_token">
            <input type="hidden" value='{{$useridentix}}' name="restore_pwtoken_userid">
            <div class="field">
              <label for="restore_pwtoken_password" class="label">Nueva contraseña</label>
              <div class="control">
                <input id='restore_pwtoken_password' name='restore_pwtoken_password' type="password" class="input">
              </div>
            </div>
            <div class="field">
              <label for="restore_pwtoken_rpassword" class="label">Repetir contraseña</label>
              <div class="control">
                <input id='restore_pwtoken_rpassword' name='restore_pwtoken_rpassword' type="password" class="input">
              </div>
            </div>

            <div class="field">
              <div class="control has-text-centered">
                <button class="button is-link">Restaurar</button>
              </div>
            </div>

          </form>
          <div class="register_actions has-text-centered">
            <a href="{{url('/auth/login')}}" class='button is-outlined'>Ingresar</a>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection