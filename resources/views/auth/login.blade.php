@extends("layouts.app")

@section("title",'Ingreso de usuarios')
@section("content")

<section class="section" id="login_section">
  <div class="login_abs_wrap">
    @if ( isset($_GET["login"]))
    @if ( $_GET["login"] == "errlog")
    <article class="message is-danger">
      <div class="message-body">
        <p>Email o contrasena incorrecta.</p>
      </div>
    </article>
    @endif
    @endif
    <div class="login_box">
      <div class="login_columns">
        <div class="login_left">
          <div class="login_form_wrap">
            <div class="login_logo">
              <img src="{{asset('images/logo.png')}}" alt="">
            </div>
            <!-- <h4 class="title is-4 has-text-link has-text-centered	">Ingreso de usuario</h4> -->
            <form method="POST" action="{{url('auth/login')}}" id="login_form">
              @csrf
              <div class="field">
                <label for="login_form_email" class="label">Email</label>
                <div class="control">
                  <input type="email" name='login_form_email' id='login_form_email' class="input">
                </div>
              </div>
              <div class="field">
                <label for="login_form_password" class="label">Contrasena</label>
                <div class="control">
                  <input type="password" name='login_form_password' id='login_form_password' class="input">
                </div>
              </div>
              <div class="field">
                <button class="button is-link">Ingresar</button>
              </div>
            </form>
            <div class="login_actions">
              <a href='{{url("auth/register")}}' class="button is-outlined">Registrarse</a>
              <a href="{{url("auth/restore-password")}}" class="button is-outlined">Restaurar Contrasena</a>
            </div>
          </div>
        </div>
        <div class="login_right">
          <div class="login_image">
            <h2 class='title font-2 is-1 has-text-light'>Nevera M&Q</h2>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

@endsection