@extends("layouts.app")

@section("title","Restablecer Contraseña")
@section("content")
{{-- Este archivo sera usado para confirmar al cliente de cambiar la contrasena y permitir que escriba el email de ingreso para enviarle el correo con token de usuario. --}}

<section class="section" id="restore_pass_section">
  <div class="register_abs_wrap">
    <div class="register_box">
      <div class="box">
        
        <h4 class="title is-4 has-text-link">Restablecer Contraseña</h4>
        <div class="register_form_wrap">
          @if (isset($message))
            <article class="message is-link">
              <div class="message-body">
                {{$message}}
              </div>
            </article>
          @endif
          <form action="{{url('auth/restore-password')}}" method="POST" id="restore_password_form">
            @csrf
            <p>Por favor, ingresa tu correo de ingreso, te enviaremos un email para restaurar la contraseña.</p>
            <div class="field">
              <label for="restore_pw_email" class="label">Email de ingreso</label>
              <div class="control">
                <input id='restore_pw_email' name='restore_pw_email' type="email" class="input">
              </div>
            </div>
            
            <div class="field">
              <div class="control has-text-centered">
                <button class="button is-link">Restaurar</button>
              </div>
            </div>

          </form>
          <div class="register_actions has-text-centered">
            <a href="{{url('/auth/login')}}" class='button is-outlined'>Ingresar</a>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection