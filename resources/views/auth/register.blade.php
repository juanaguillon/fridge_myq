@extends("layouts.app")

@section("title","Registro de usuario")
@section("content")

<section class="section" id="register_section">
  <div class="register_abs_wrap">
    <div class="register_box">
      <div class="box">
        <article id="register_message_error" class="message is-danger hidden">
          <div class="message-body">
            <p id="message_error_data">Error</p>
          </div>
        </article>

        @if ( isset($_GET["newreg"]))
        @if ( $_GET["newreg"] == "success")
        <article class="message is-success">
          <div class="message-body">
            <p>Se ha creado correctamente el usuario.</p>
          </div>
        </article>
        @else
        <article class="message is-danger">
          <div class="message-body">
            <p>No se ha logrado registrar el nuevo usuario. Intente nuevamente</p>
          </div>
        </article>
        @endif
        @endif

        @if ($errors->any())
          @foreach( $errors->all() as $error)
          <div class='message is-danger'>
            <div class="message-body">
             {{$error}}
            </div>
          </div>
          @endforeach
        @endif
        <h4 class="title is-4 has-text-link">Registro de usuario</h4>
        <div class="register_form_wrap">
          <form action="{{url('auth/register')}}" method="POST" id="register_form">
            @csrf
            <div class="field">
              <label for="register_user_cedula" class="label">Cedula</label>
              <div class="control">
                <input id='register_user_cedula' name='register_user_cedula' type="number" class="input">
              </div>
            </div>
            <div class="field">
              <label for="register_user_name" class="label">Nombre y apellido</label>
              <div class="control">
                <input id="register_user_name" name='register_user_name' type="text" class="input">
              </div>
            </div>
            <div class="field">
              <label for="register_user_email" class="label">Email</label>
              <div class="control">
                <input id="register_user_email" name='register_user_email' type="email" class="input">
              </div>
            </div>
            <div class="field">
              <label for="register_user_password" class="label">Contrasena</label>
              <div class="control">
                <input id="register_user_password" name='register_user_password' type="password" class="input">
              </div>
            </div>
            <div class="field">
              <label for="register_user_rpassword" class="label">Repetir Contrasena</label>
              <div class="control">
                <input id="register_user_rpassword" name='register_user_rpassword' type="password" class="input">
              </div>
            </div>
            <div class="field">
              <div class="control has-text-centered">
                <button class="button is-link">Registrar</button>
              </div>
            </div>

          </form>
          <div class="register_actions has-text-centered">
            <a href="{{url('/auth/login')}}" class='button is-outlined'>Ingresar</a>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection