@extends('layouts.section')
@section('title','Mis detalles')
@section('section_id', 'details_section')
@section('subcontent')


  @if ( count( $sells ) > 0 )
    @component('components.item_sell_by_user', ["show_button_sald" => false])

      @slot('user_email', session()->get('user_email') )
      @slot('user_name', session()->get('user_name') )
      @slot('user_id', session()->get('user_id') )
      @slot('user_total', $total )
        @foreach ( $sells as $sell )
        <div class="column is-12-mobile is-3-widescreen is-4-desktop is-6-tablet">
          @component('components.item_sell', array(
          'sell_id' => $sell->sell_id,
          'sell_title' => $sell->prod_nombre,
          'sell_image' => $sell->prod_image,
          'sell_price' => $sell->sell_price,
          'sell_quantity' => $sell->sell_quantity,
          'sell_off' => $sell->prod_off,
          'sell_status' => $sell->sell_status,
          'show_button_sald' => false
          ))
          @endcomponent
        </div>
        @endforeach

    @endcomponent
    <div class="close_filter_wrap">
      <button data-today='{{date('Y-m-d', time())}}' data-token='{{csrf_token()}}' data-current_user='{{session()->get('user_id')}}' id='delete_filters_sells' class="button is-small is-link">Eliminar Filtro</button>
      
    </div>
    
    <div class="box">
      <h4 class='title is-4 has-text-link has-text-centered'>Filtrar compras por fecha</h4>
      <div class="columns">
        <div class="column is-6">
          <div class="field">
            <label for="sells_details_start" class="label">Desde</label>
            <input type="date" min='2019-03-10' max='{{date('Y-m-d', time())}}' value='2019-03-10' id="sells_details_start" class='input'>
          </div>
        </div>
        <div class="column is-6">
          <div class="field">
            <label for="sells_details_end" class="label">Hasta</label>
            <input type="date" min='2019-03-10' max='{{date('Y-m-d', time())}}' value='{{date('Y-m-d', time())}}' id="sells_details_end" class='input'>
          </div>
        </div>
      </div>
      <div class="has-text-centered">

        <button data-token='{{csrf_token()}}' data-current_user='{{session()->get('user_id')}}' class="button is-link" id="details_filter">Filtrar</button>
      </div>
    </div>

  @else
      <div class="column is-12">
        <div class="box">
          <h4 class='title is-5 has-text-link'>Actualmente no tienes compras registradas</h4>
        </div>
      </div>
  @endif


@endsection