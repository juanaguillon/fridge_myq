@extends('layouts.section')
@section('title','Detalles de ventas')
@section('section_id', 'general_details_section')
@section('subcontent')

<div class="edit_products_container">
  <div class="columns is-multiline">
    <div class="column is-12-mobile is-4-tablet">
      <div class="box_info has-text-centered">
        <h4 class='title is-6 is-marginless has-text-link'>Total Vendido</h4>
        <span>{{ formatMoney($admin_data['total']) }}</span>
      </div>
    </div>
    <div class="column is-12-mobile is-4-tablet">
      <div class="box_info has-text-centered">
        <h4 class='title is-6 is-marginless has-text-link'>Total Pagos</h4>
        <span>{{ formatMoney($admin_data['pagado']) }}</span>
      </div>
    </div>
    <div class="column is-12-mobile is-4-tablet">
      <div class="box_info has-text-centered">
        <h4 class='title is-6 is-marginless has-text-link'>Total Deudas</h4>
        <span>{{ formatMoney($admin_data['saldo']) }}</span>
      </div>
    </div>
    <div class="column is-12">
      <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th class="has-text-link">Usuario</th>
            <th class="has-text-link">Total Compras</th>
            <th class="has-text-link">Total Pagado</th>
            <th class="has-text-link">Total Deuda</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($users_data as $data) :  ?>
          <tr>
            <td>{{ formatUserName($data->user_name)}}</td>
            <td>{{ formatMoney($data->total)}}</td>
            <td>{{ formatMoney($data->pagado)}}</td>
            <td>{{ formatMoney($data->saldo)}}</td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>

  </div>




</div>

@endsection