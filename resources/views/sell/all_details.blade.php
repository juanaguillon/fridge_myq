@extends('layouts.section')
@section('title','Detalles de ventas')
@section('section_id', 'general_details_section')
@section('subcontent')
{{-- $all_sells = SellerController::getAllSells() --}}
@if ( count( $all_sells ) > 0 )
<div id="search_by_user">

  <div class="field search_by_user_wrap">
    <article id='no_search_passed' class="hidden message is-danger">
      <div class="message-body">
        Debe ingresar un texto para buscar.
      </div>
    </article>
    <h3 class="title is-4 has-text-link">
      Buscar por usuario
    </h3>
    <input placeholder="Nombre de usuario" type="text" id='search_by_user_input' class="input">
  </div>
  <button id='search_by_user_button' data-token="{{csrf_token()}}" class="button is-link">Buscar</button>
</div>


<div id="all_details_wrap">
  @foreach($all_sells as $sell )

  @if ( count( $sell["sells"] ) > 0 )
  @component('components.item_sell_by_user', ["show_button_sald" => true])
  @slot('user_email', $sell['user_email'] )
  @slot('user_name', $sell['user_name'])
  @slot('user_id', $sell['user_id'])
  @slot('user_total', $sell['total_deuda'])

  @foreach ( $sell["sells"] as $sell )
  <div class="column is-12-mobile is-3-widescreen is-4-desktop is-hiddable is-6-tablet sell_item_id_{{$sell->sell_id}}">
    @component('components.item_sell', array(
    'sell_id' => $sell->sell_id,
    'sell_title' => $sell->prod_nombre,
    'sell_image' => $sell->prod_image,
    'sell_price' => $sell->sell_price,
    'sell_quantity' => $sell->sell_quantity,
    'sell_off' => $sell->prod_off,
    'sell_status' => $sell->sell_status,
    "show_button_sald" => true
    ))
    @endcomponent
  </div>
  @endforeach
  @endcomponent
  @endif

  @endforeach
</div>

<div class="close_filter_wrap">
  <button data-token='{{csrf_token()}}' id='delete_filters_all_sells' class="button is-small is-link">Eliminar
    Filtro</button>

</div>

<div class="box">
  <h4 class='title is-4 has-text-link has-text-centered'>Filtrar compras por fecha</h4>
  <div class="columns">
    <div class="column is-6">
      <div class="field">
        <label for="sells_details_start" class="label">Desde</label>
        <input type="date" min='2019-03-10' max='{{date('Y-m-d', time())}}' value='2019-03-10' id="sells_details_start"
          class='input'>
      </div>
    </div>
    <div class="column is-6">
      <div class="field">
        <label for="sells_details_end" class="label">Hasta</label>
        <input type="date" min='2019-03-10' max='{{date('Y-m-d', time())}}' value='{{date('Y-m-d', time())}}'
          id="sells_details_end" class='input'>
      </div>
    </div>
  </div>
  <div class="has-text-centered">
    <button data-token='{{csrf_token()}}' data-current_user='{{session()->get('user_id')}}' class="button is-link"
      id="all_details_filter">Filtrar</button>
  </div>
</div>
@else
<div class="box">
  <h4 class="is-subtitle has-text-link">No existen ventas pendientes</h4>
</div>
@endif


@component('components.modal_confirm', array(
"title" => 'Saldar venta',
"text" => "Está seguro/a de saldar esta venta?",
"continue" => "Saldar",
"cancel" => "Cancelar"
))
@endcomponent


<div id="confirm_sells_user" class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Saldar ventas</p>
      <button class="delete" aria-label="close"></button>
    </header>
    <section class="modal-card-body">
      <p>Desea saldar todas las ventas de este usuario?</p>
    </section>
    <footer class="modal-card-foot">
      <button id="confirm_button_sells_user" class="button is-success">Saldar Ventas</button>
      <button class="button is-danger close_modal">Cancelar</button>
    </footer>
  </div>
</div>

<div id="confirm_sells_user_message" class="modal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <div class="box has-text-centered">
      <p id='confirm_sells_user_message_text'></p>
      <button class="button is-dark close_modal" aria-label="close">Entendido</button>
    </div>
  </div>
</div>


@endsection