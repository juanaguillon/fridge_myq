@extends('layouts.section')
@section('title','Detalles de ventas')
@section('section_id', 'general_details_section')
@section('subcontent')
<div class="box box_mini">
  <h3 class="title has-text-link is-4">
    Editar Venta
  </h3>
  <div class="edit_sell_wrap">
    <div id='edit_sell_message_error' class="message is-danger hidden">
      <div class="message-body">
        La cantidad no puede estar vacia.
      </div>
    </div>
    <div id='edit_sell_message_success' class="message is-success hidden">
      <div class="message-body">
        Venta guardada correctamente.
      </div>
    </div>

    <form id="edit_sell_form">
      <input type="hidden" value="{{$sell->sell_id}}" id='actual_sell'>
      <input type="hidden" value="{{csrf_token()}}" id='_tokenize'>
      <div class="field">
        <label for="edit_sell_quantity" class="label">Actual Producto</label>
        <input type="text" disabled value='{{$product->prod_nombre}}' class="input">
      </div>
      <div class="field">
        <span class="has-text-link">{{ formatMoney( $sell->sell_price) }}</span>
      </div>
      <div class="field">
        <label for="edit_sell_quantity" class="label">Cantidad</label>
        <input type="number" value='{{$sell->sell_quantity}}' class="input" id="edit_sell_quantity">
      </div>
      
      <div class="field is-grouped">
        <label for="edit_sell_product" class="label">¿Está sin saldar?</label>
        <div class="custom_checkbox" style="margin:0 8px">
          @php
          $checked = '';
          if ( $sell->sell_status){
          $checked = 'checked';
          }
          @endphp
          <input {{$checked}} type="checkbox" id="edit_sell_product">
          <i class="icon-checkmark"></i>
        </div>
        <small>Verdadero - Esta pendiente por saldar.</small>
      </div>
      <div class="field">
        <button class="button is-link">Guardar</button>
      </div>

    </form>

  </div>
</div>

@endsection