<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield("title")</title>
  <link href="https://fonts.googleapis.com/css?family=Barlow|Nunito|Rubik&display=swap" rel="stylesheet">


  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

</head>

<body>
  @if ( session()->exists('user_id') )
  <nav class="navbar is-link" role="navigation" aria-label="main navigation">
    <div class="container">
      <div class="navbar-brand">

        <a role="button" id='show_menu_mobile' class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div id="navbar_fridge" class="navbar-menu">
        <div class="navbar-start">

          <a href="{{url('product/all')}}" class="navbar-item">
            Productos
          </a>
          @if (session()->get('user_rol') === 'admin')
            <div class="navbar-item has-dropdown is-hoverable">
              <a class="navbar-link">
                Administrar
              </a>

              <div class="navbar-dropdown">
                <a href="{{url('product/create')}}" class="navbar-item">
                  Crear Producto
                </a>
                <a href={{url('product/edit-products')}} class="navbar-item">
                  Editar Productos
                </a>
                <hr class="navbar-divider">
                <a href={{url("sell/general-details")}} class="navbar-item">
                  Administrar Ventas
                </a>
                <a href="{{url('user/get-users')}}" class="navbar-item">
                  Administrar Usuarios
                </a>
                <a href="{{url('sell/get-historic-sells')}}" class="navbar-item">
                  Histórico de Ventas
                </a>

                </a>
              </div>
            </div>
          @endif
        </div>
        <div class="navbar-end">
          <div class="navbar-item has-dropdown  is-hoverable">
            <a class="navbar-link">
              @php
              $userName = explode(' ', session()->get('user_name'))[0];
              @endphp
              {{ $userName }}
            </a>

            <div class="navbar-dropdown is-right">
              <a href="{{url('sell/my-details')}}" class="navbar-item">
                <span>Mis Detalles</span>
              </a>
              <a href={{url('user/settings')}} class="navbar-item">
                <span> Mi Configuracion</span>
              </a>
              <hr class="navbar-divider">
              <a href='{{url("auth/exit")}}' class="navbar-item">
                <span>Cerrar Sesion</span>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>

  </nav>
  @endif

  <div class="container">
    @yield("content")
  </div>

  <script src="{{asset('assets/app.js')}}"></script>
</body>

</html>