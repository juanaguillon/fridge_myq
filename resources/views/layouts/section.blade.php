@extends ("layouts.app")

@section('content')
<section class="section" id="@yield('section_id', 'generic_section')">
  <div class="section_container">
    @yield("subcontent")
  </div>
</section>

@endsection