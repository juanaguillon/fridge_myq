<p>Este correo ha sido enviado ya que has solicitado cambiar la contraseña de cuenta en <a
    href="{{env('APP_URL')}}">Nevera MYQ</a>. <br>
  <strong>Debes tener en cuenta que solo podras ingresar a este link durante los 30 próximos minutos después de enviado este email.</strong></p>
<p>Si has solicitado este cambio, entra el siguiente <a class='button'
    href="{{env('APP_URL')}}/auth/restore-password-tokenized/{{$userID}}/{{$userToken}}">link</a> para generar tu nueva
  contraseña.</p>
<p>Si no has soliticado este cambio, comunícate inmediatamente con el administrador de la aplicación.<br><strong>El equipo MYQ</strong></p>