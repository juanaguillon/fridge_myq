import sd, { _fn } from "../../singleDom";
import { EnvironmentPlugin } from "webpack";

/**
 * Agregar un nuevo producto a las ventas del usuario actual.
 */
function addNewProductToSells() {
  sd(".add_sell_button").click(function (e, elm) {
    sd("#modal_confirm").addClass("is-active");
    sd("#close_modal_confirm").click(function (ev, elmt) {
      sd("#modal_confirm").removeClass("is-active");
    });

    sd("#confirm_modal_confirm").unbind()
    sd("#confirm_modal_confirm").click(function (ev, elmt) {
      var product = elm.getAttribute("data-prod");
      var price = elm.getAttribute("data-price");
      var quantity = sd(elm)
        .parent()
        .sibling("w-100")
        .children("input")
        .val();

      _fn.__ajax({
        url: process.env.BACKEND_URL + "/sell/create",
        method: "POST",
        data: {
          sell_prod: product,
          sell_price: price,
          sell_quantity: quantity,
          _token: sd("#csrf_token").val()
        },
        success: function (data) {
          console.log(data)
          sd("#modal_confirm").removeClass("is-active");
          if (data.newsell) {
            sd("#modal_message_text").text(
              "Se ha agregado correctamente el producto"
            );
          } else {
            sd("#modal_message_text").text(
              "Error al agregar el producto, intente nuevamente"
            );
          }
          sd("#modal_message").addClass("is-active");
          sd("#close_modal_message").click(function () {
            sd("#modal_message").removeClass("is-active");
          });
          setTimeout(() => {
            sd("#modal_message").removeClass("is-active");
          }, 5000);
        }
      });
    });
  });
}

/**
 * Buscar productos por nombre de producto
 */
function searchProductsByName() {

  var executeSearch = () => {
    let inputVal = sd('#search_by_product_input').val();
    if (inputVal === '') {
      sd('#no_search_passed').removeClass('hidden')
    } else {
      sd('#no_search_passed').addClass('hidden');
      let url = process.env.BACKEND_URL + '/product/get-by-name/' + inputVal
      _fn.__ajax({
        url: url,
        method: 'GET',
        success: data => {
          sd('#all_products_container').html(data['view'])
          addNewProductToSells();
        }
      })
    }
  }

  sd('#search_by_product_button').click(executeSearch)
  sd('#search_by_product_input').keydown((e) => {
    if (e.keyCode === 13) {
      executeSearch();
    }
  })
}
/** Mostrar todos los productos */
sd('#search_all_products').click(e => {
  _fn.__ajax({
    url : process.env.BACKEND_URL + "/product/all-ajax",
    method: 'GET',
    success: e =>{
      sd('#all_products_container').html(e['views'])
      addNewProductToSells();
    }
  })
})


searchProductsByName();
addNewProductToSells()