import sd, { _fn } from '../../singleDom';

/**
	 * Verificador de registro de nuevo producto.
	 */
var image = sd("#create_product_image");
image.change(function (e, elm) {
  _fn.createBase64(elm.files[0], dt => {
    console.log(sd("#view_image_prod").attr("src", dt));
  });
  if (elm.files.length > 0) {
    sd("#img_name").text(elm.files[0].name);
  } else {
    sd("#img_name").text("Vacio");
  }
});

sd("#create_product_form").submit(function (e) {
  var title = sd("#create_product_title").val();
  var image = sd("#create_product_image");
  var price = sd("#create_product_price").val();
  var showError = function (errorMessage) {
    e.preventDefault();
    sd("#product_message_error").removeClass("hidden");
    sd("#message_error_data").text(errorMessage);
  };
  if (!title) {
    showError("El nombre del producto es obligatiorio");
  } else if (!price) {
    showError("El precio del producto es obligatiorio");
  } else if (image.activeObject[0].files.length === 0) {
    if (!image.hasClass("omit_verify")) {
      showError("Debe subir una imagen del producto");
    }
  }
});