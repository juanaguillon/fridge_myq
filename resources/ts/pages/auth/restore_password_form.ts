import sd from '../../singleDom';

sd('#restore_passtoken_form').submit((e:Event) =>{

  var password = sd('#restore_pwtoken_password').val()
  var rpassword = sd('#restore_pwtoken_rpassword').val()
  if ( password !== rpassword ){
    e.preventDefault()
    sd('#restore_pwtoken_message').removeClass('hidden')
  }
});