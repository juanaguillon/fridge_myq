import sd from '../../singleDom';

/**
	 * Verificador de registro de usuario.
	 */
sd("#register_form").submit(function (e, elem) {
  var cedula = sd("#register_user_cedula").val(),
    name = sd("#register_user_name").val(),
    email = sd("#register_user_email").val(),
    pass = sd("#register_user_password").val(),
    rpass = sd("#register_user_rpassword").val(),
    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  var showError = function (text) {
    e.preventDefault();
    sd("#register_message_error").removeClass("hidden");
    sd("#message_error_data").text(text);
  };

  if (!cedula || !name || !email || !pass || !rpass) {
    showError("Todos los campos son requeridos");
  } else if (cedula.length < 6 || cedula.length > 14) {
    showError("El numero de cedula es incorrecto");
  } else if (!emailReg.test(email)) {
    showError("El formato de email es incorrecto.");
  } else if (pass !== rpass) {
    showError("Las contraenas no coinciden");
  }
});