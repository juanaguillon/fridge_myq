import sd, { _fn } from '../../singleDom';

sd('#edit_sell_form').submit((e, elm) => {
  e.preventDefault();
  let quantity = sd('#edit_sell_quantity').val();
  let checked = sd('#edit_sell_product').activeObject[0].checked;
  let actualSell = sd('#actual_sell').val()
  let token = sd('#_tokenize').val()
  if (quantity === '') {
    sd('#edit_sell_message_error').removeClass('hidden');
    return;
  }
  _fn.__ajax({
    method: 'POST',
    url: process.env.BACKEND_URL + '/sell/edit-sell/' + actualSell,
    data: {
      sell_quantity: quantity,
      sell_status: checked,
      _token: token
    },
    success: data => {
      if ( data['success'] === true){
        sd('#edit_sell_message_error').addClass('hidden');
        sd('#edit_sell_message_success').removeClass('hidden');
      }else{
        alert(data['message'])
      }
    }
  })
})
