import datepicker from 'js-datepicker'
import sd, { _fn } from '../../singleDom';

function checkInput(type) {
  var input = document.createElement("input");
  input.setAttribute("type", type);
  return input.type == type;
}

/**
 * Verificar si el navegador soporta el input type=date.
 * En caso que no lo soporte, se agregara el plugin para mostrar el calendario.
 */
if (!checkInput('date')) {


  if (document.getElementById('sells_details_start')) {
    sd('#sells_details_start,#sells_details_end').attr('type', 'text')

    let objDate = {
      customDays: ["Lun", 'Mar', 'Mier', 'Jue', 'Vier', 'Sab', 'Dom'],
      startDay: 0,
      id: 1,
      minDate: new Date(2019, 2, 10),
      maxDate: new Date(),
      formatter: (input, date, instance) => {
        console.log(date.toLocaleDateString())
        input.value = date.toLocaleDateString()
      },
    }
    let dateStart = datepicker('#sells_details_start', objDate);
    let dateEd = datepicker('#sells_details_end', objDate);
    dateStart.getRange()
    dateEd.getRange()

  }
}

/**
 * Filtrar por fecha.
 */
sd('#details_filter').click((e, elm: HTMLElement) => {
  _fn.__ajax({
    url: process.env.BACKEND_URL + "/sell/sells-by-date",
    method: "POST",
    success: data => {
      sd('.close_filter_wrap').addClass('show');
      sd('.sell_user_total').text(data['total'])
      sd('.sell_container_clms').html(data['view'])
    },
    // json:false,
    data: {
      "date_from": sd('#sells_details_start').val(),
      "_token": sd(elm).data('token'),
      "date_to": sd('#sells_details_end').val(),
      "date_user": sd(elm).data('current_user')
    }
  })
});

/**
 * Eliminar el filtro por fecha.
 */
sd('#delete_filters_sells').click((e, elm) =>{
  _fn.__ajax({
    url: process.env.BACKEND_URL + "/sell/sells-by-date",
    method: "POST",
    success: data => {
      
      sd(elm).parent().removeClass('show')
      sd('.sell_user_total').text(data['total'])
      sd('.sell_container_clms').html(data['view'])
    },
    // json: false,
    data: {
      "date_from": '2019-03-10',
      "_token": sd(elm).data('token'),
      "date_to": sd(elm).data('today'),
      "date_user": sd(elm).data('current_user')
    }
  })
})
