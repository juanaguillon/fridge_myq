import sd, { _fn } from '../../singleDom';
import { disableSingleSell } from '../components/item_sell';


/**
 * Filtrar por nombre de usuario
 */

const searchDetailsByName = (e, elmt) => {

  let inputVal = sd('#search_by_user_input').val()
  if (inputVal === '') {
    sd('#no_search_passed').removeClass('hidden')
    return;
  }
  _fn.__ajax({
    'url': process.env.BACKEND_URL + '/sell/search-by-name',
    'method': 'POST',
    'success': data => {

      sd('#all_details_wrap').html(data['view'])
      disabeAllUserSells()
      disableSingleSell();
    },
    'data': {
      _token: sd('#search_by_user_button').data('token'),
      user_name: sd('#search_by_user_input').val()
    }
  })
}

sd('#search_by_user_button').click((e, elemnt) => {
  searchDetailsByName(e, elemnt)
});
sd('#search_by_user_input').keydown((e, elemnt) => {
  if (e.keyCode == 13) {
    searchDetailsByName(e, elemnt)
  }
});

/**
 * Agregar el evento click para remover todas las ventas de un usuario en especifico
 */
function disabeAllUserSells() {
  sd('.close_modal').click(() => sd(".modal").removeClass('is-active'))
  sd('.sald_all_sells_user').click((e, elmtG) => {

    sd("#confirm_sells_user").addClass('is-active')

    var userToSald = sd(elmtG).data('user_sald');


    /** Si se da click en confirmar las ventas */
    sd('#confirm_button_sells_user').unbind()
    sd("#confirm_button_sells_user").click(() => {
      _fn.__ajax({
        url: process.env.BACKEND_URL + '/sell/disable-user-sells',
        success: (data) => {
          sd('#confirm_sells_user').removeClass('is-active')
          sd('#confirm_sells_user_message').addClass('is-active')
          if (data['success']) {
            sd('#confirm_sells_user_message_text').text(data['message'])
            sd('#details_abs_' + userToSald).addClass('hidden')
          } else {
            sd('#confirm_sells_user_message_text').text(data['message'])
          }
        },
        data: {
          sell_user: sd(elmtG).data('user_sald'),
          _token: sd(elmtG).data('token')
        },
        method: "POST"
      })
    })

  });
}


/**
 * Filtrar por fecha.
 */
sd('#all_details_filter').click((e, elm: HTMLElement) => {
  _fn.__ajax({
    url: process.env.BACKEND_URL + "/sell/all-sells-by-date",
    method: "POST",
    success: data => {
      sd('.close_filter_wrap').addClass('show');
      sd('#all_details_wrap').html(data['view'])
    },
    // json:false,
    data: {
      "date_from": sd('#sells_details_start').val(),
      "_token": sd(elm).data('token'),
      "date_to": sd('#sells_details_end').val(),
    }
  })
});

/**
 * Eliminar el filtro por fecha.
 */
sd('#delete_filters_all_sells').click((e, elm) => {
  _fn.__ajax({
    url: process.env.BACKEND_URL + "/sell/general-details",
    method: "POST",
    success: data => {

      sd(elm).parent().removeClass('show')
      sd('#all_details_wrap').html(data['view'])
    },
    // json: false,
    data: {
      "_token": sd(elm).data('token'),
    }
  })
});

window.onload = () => {
  disabeAllUserSells();
}