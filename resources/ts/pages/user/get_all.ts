import sd, {_fn}  from '../../singleDom';
sd('.button_toggle_user').click((e,elm)=>{

  let actualButton = sd(elm);
  _fn.__ajax({
    url: process.env.BACKEND_URL + '/user/toggle-user-status',
    method: 'POST',
    data: {
      _token: sd('#_token_id').val(),
      user_id_to_toggle: actualButton.data('user_toggle')
    },
    success: data =>{
      
      if ( data.success){
        if (actualButton.hasClass("is-danger")){
          actualButton.text('Habilitar')
          actualButton.removeClass('is-danger')
          actualButton.addClass('is-success')
        }else{
          actualButton.text('Deshabilitar')
          actualButton.removeClass('is-success')
          actualButton.addClass('is-danger')
        }
        
      }
    } 
  })
});