import sd, {_fn} from '../../singleDom';

sd('#change_password_checker').change((e, elm) =>{
  console.log(sd(elm).activeObject[0])
  if (  sd(elm).activeObject[0].checked ){
    sd('.need_password_checker').removeClass('hidden');
    sd('.need_password_checker').addClass('show');
  }else{
    sd('.need_password_checker').addClass('hidden');
    sd('.need_password_checker').removeClass('show');
  }
})