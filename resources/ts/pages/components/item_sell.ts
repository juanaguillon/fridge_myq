import sd, { _fn } from '../../singleDom';

sd("#close_modal_message").click(function () {
  sd("#modal_message").removeClass('is-active')
});

/**
 * Deshabilitar una venta en especifico.
 */
function disableSingleSell() {
  sd(".sell_sald_button").click(function (e, element) {
    sd("#modal_confirm").addClass("is-active");
    sd("#close_modal_confirm").click(function () {
      sd("#modal_confirm").removeClass("is-active");
    });
    let modalConfirmFunction = (ev, elmt) => {
      let saldId = sd(element).data('sald')
      let tokenID = sd(element).data('token')
      _fn.__ajax({
        url: process.env.BACKEND_URL.toString() + "/sell/disable-sell",
        method: 'POST',
        data: {
          _token: tokenID,
          sell_id: saldId
        },
        success: function (data) {
          console.log(data)
          sd("#modal_confirm").removeClass('is-active')
          sd("#modal_message").addClass('is-active')


          if (data.success) {
            let itemID = sd(".sell_item_id_" + saldId)
            itemID.addClass('hide');
            setTimeout(() => {
              itemID.addClass('remove')
            }, 500);
            sd('#modal_message_text').text('Se ha saldado la venta correctamente')
          } else {
            sd('#modal_message_text').text('Error intenrno, intente nuevamente')
          }
        }
      })
    }

    sd("#confirm_modal_confirm").unbind();
    sd("#confirm_modal_confirm").click(modalConfirmFunction);
  });
}

disableSingleSell()
export {
  disableSingleSell 
}
