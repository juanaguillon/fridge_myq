import sd, {_fn} from './singleDom';
import './pages/auth';
import './pages/product';
import './pages/components';
import './pages/sells';
import './pages/user';

sd('#show_menu_mobile').click((e,elm)=>{
  e.preventDefault();
  sd(elm).toggleClass('is-active')
  sd('#navbar_fridge').toggleClass('is-active')
})