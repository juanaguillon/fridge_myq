/**
 * Este archivo se ha creado con el fin de intentar hacer una implementacion similar a Jquery, simplemente con mucho menos metodos y funciones, ya que solo se busca crear funciones que estan usando en el proyecto.
 * Puede revisar todos los metodos, los cuales puede usar y llamarlos de manera similar ( Si no es igual ) a la libreria real de jQuery ( v.3.4.1)
 */

interface ajaxInterface {
  url: string,
  method: string,
  success: Function,
  data?: any,
  json?: boolean
}


const _fn = {
  __loopOfElements: function (elements, callback) {
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      callback(element);
    }
  },
  __loopOfElementsOnEvent: function (event, elements, callback) {
    this.__loopOfElements(elements, function (element) {
      element.addEventListener(event, function (e) {
        callback(e, this);
      });
    });
  },
  __ajax: function (options: ajaxInterface) {
    var xhttp = new XMLHttpRequest();
    xhttp.open(options.method, options.url, true);
    if (typeof options.data == "undefined") {
      xhttp.send();
    } else {
      var formData = new FormData();
      for (var key in options.data) {
        if (options.data.hasOwnProperty(key)) {
          var val = options.data[key];
          formData.append(key, val);
        }
      }
      xhttp.send(formData);
    }
    xhttp.onreadystatechange = function () {
      if (xhttp.readyState === xhttp.DONE) {
        if (xhttp.status == 200) {
          if (typeof options.json === 'undefined' || options.json) {
            options.success(JSON.parse(xhttp.responseText));
          } else {
            options.success(xhttp.responseText)
          }
        } else if (xhttp.readyState == 400) {
          alert("Error exists number 400");
        } else {
          alert("Internal server error, unknown");
        }
      }
    };
  },
  __hasClass: function (elem, className) {
    return (" " + elem.className + " ").indexOf(" " + className + " ") > -1;
  },
  getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2)
      return parts
        .pop()
        .split(";")
        .shift();
  },
  createBase64(file, onLoad) {
    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => {
      onLoad(reader.result);
    };
  }


}

function createEvent(eventName, element, callback) {
  _fn.__loopOfElementsOnEvent(eventName, element, function (e, actualElem) {
    callback(e, actualElem);
  });
}

class single_dom {

  activeObject;
  _fn: any;
  isSingle: boolean;

  constructor(elementCursor) {
    this._fn = _fn;
    this.isSingle = false;
    if (elementCursor instanceof HTMLElement) {
      this.activeObject = [elementCursor]
    } else if (document.querySelector(elementCursor)) {
      this.activeObject = document.querySelectorAll(elementCursor)
    }

    if (this.exists() && this.activeObject.length == 1) {
      this.isSingle = true
    }
  }

  hasClass(className) {
    if (this.exists()) {
      return this._fn.__hasClass(this.activeObject[0], className);
    }
  };
  exists() {
    if (this.activeObject) {
      if (this.activeObject.constructor === Array) {
        return this.activeObject.length > 0;
      } else {
        return typeof this.activeObject !== "undefined";
      }
    } else {
      return false;
    }
  };
  val() {
    if (this.exists()) {
      return this.activeObject[0].value;
    } else {
      return false;
    }
  };
  text(newText) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function (element) {
        element.textContent = newText;
      });
    }
  };
  html(newHTML: string) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function (element) {
        element.innerHTML = newHTML;
      });
    }
  }
  append(element) {
    if (this.exists()) {
      _fn.__loopOfElements(this.activeObject, function (elm) {
        elm.innerHTML = elm.innerHTML + element;
      });
    }
  };
  remove() {
    if (this.exists()) {
      if (this.isSingle) {
        this.activeObject[0].parentNode.removeChild(this.activeObject[0]);
      } else {
        this._fn.__loopOfElements(this.activeObject, function (element) {
          element.parentNode.removeChild(element);
        });
      }
    }
  };
  addClass(newClassName) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function (elm) {
        elm.classList.add(newClassName);
      });
    }
  };
  toggleClass(toggleClassName) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function (elm) {
        elm.classList.toggle(toggleClassName);
      });
    }
  };
  removeClass(removeClassName) {
    if (this.exists()) {
      this._fn.__loopOfElements(this.activeObject, function (elm) {
        elm.classList.remove(removeClassName);
      });
    }
  };

  attr(attr, newAttr = null) {
    if (this.exists()) {
      if (!newAttr) {
        return this.activeObject[0].getAttribute(attr);
      } else {
        _fn.__loopOfElements(this.activeObject, function (elm) {
          elm.setAttribute(attr, newAttr);
        });
      }
    }
  };

  click(callback) {
    if (this.exists()) {
      createEvent("click", this.activeObject, callback);
    }
  };

  keyup(callback) {
    if (this.exists()) {
      createEvent("keyup", this.activeObject, callback);
    }
  }
  keydown(callback) {
    if (this.exists()) {
      createEvent("keydown", this.activeObject, callback);
    }
  }
  submit(callback) {
    if (this.exists()) {
      createEvent("submit", this.activeObject, callback);
    }
  };
  change(callback) {
    if (this.exists()) {
      createEvent("change", this.activeObject, callback);
    }
  };
  parent() {
    if (this.exists()) {
      this.activeObject = [this.activeObject[0].parentNode];
      return this;
    }
  };
  sibling(selector) {
    if (this.exists()) {
      if (this.activeObject.constructor === Array) {
        var arrChids = this.activeObject[0].parentNode.childNodes;
      } else {
        var arrChids = this.activeObject.parentNode.childNodes;
      }
      var matchers = [];
      for (var i = 0; i < arrChids.length; i++) {
        var elmmatchet = arrChids[i];
        if (_fn.__hasClass(elmmatchet, selector)) {
          matchers.push(elmmatchet);
        }
      }

      this.activeObject = matchers;
      return this;
    }
  };

  /**
   * Get a data attribute from active elements.
   * @param {string} dataSelector Data select of element 
   * @example
   * var element = document.querySelector('#element')
   * element.setAttribute('select', 'aValueToReturn')
   * return sd(element).data('select')
   * // output: aValueToReturn
   */
  data(dataSelector: string) {
    if (this.exists()) {
      return this.activeObject[0].getAttribute('data-' + dataSelector)
    }
  }

  /**
   * Delete all events listeners of active elements
   */
  unbind(): void {
    if (this.exists()) {
      _fn.__loopOfElements(this.activeObject, (elmt) => {
        let elClone = elmt.cloneNode(true);
        elmt.parentNode.replaceChild(elClone, elmt);
      })
    }
  }

  children(selector) {
    var _this = this;
    if (selector === void 0) {
      selector = "";
    }
    if (this.activeObject.length === 1) {
      var match = this.activeObject[0];
      var matchs_1 = [];
      this._fn.__loopOfElements(match.childNodes, function (elm) {
        if (elm.nodeType === 1) {
          matchs_1.push(elm);
        }
      });
      this.activeObject = matchs_1;
    } else {
      var match_1 = [];
      for (var i = 0; i < this.activeObject.length; i++) {
        var element = this.activeObject[i];
        this._fn.__loopOfElements(element.childNodes, function (elem) {
          if (selector != "") {
            if (_this._fn.__hasClass(elem, selector)) {
              match_1.push(elem);
            }
          } else {
            if (elem.nodeType === 1) {
              match_1.push(elem);
            }
          }
        });
      }
      this.activeObject = match_1;
    }
    return this;
  };

}
export { _fn };


export default function sd(selector) {
  return new single_dom(selector);
}

