# Nevera MYQ

Toda persona interesada en modificar y contribuir en este proyecto debe tener en cuenta
que:

- Esta desarrollado en base a el framework PHP Laravel 6.0
- El javascript se esta compilando gracias a TypeScript.
- El css se compila ejecutando el lenguaje SCSS
- Se esta usando webpack para la importancion de archivos front ( JS, CSS, Fonts, etc ).
- Se usa NPM ( Node Package Manager ) para importar las dependecias JS.

Puede revisar el `composer.json`, `package.json` para conocer las dependencias.

### Requisitos

+ Tener instalado [NodeJS](https://nodejs.org/en/download/) para dependencias js ( y funcionamiento webpack)
+ Tener instalado [Composer](https://getcomposer.org/download/) para dependencias PHP ( Y funcionamiento Laravel )

### Pasos para Editar

- Clonar proyecto
- Instalar dependencias JS ( `npm install` )
- Instalar dependencias PHP ( `php composer.phar install` )
- Crear archivos de entorno de desarrollo ( Estos no pueden ser subidos al repositorio por ser credenciales de servidor. )
... Crear `.env`, donde podra ver `.env.example` para ver los valores a tener en cuenta.
... Crear `webpack.env`, donde podra ver `webpack.example.env` para ver valores a tener en cuenta
- Ejcutar comando para crear un api_key (`php artisan key:generate`)
- Crear una base de datos y agregar las credenciales de la DB en el archivo .env
- Ejecutar comando para agregar datos a la base de datos ( php artisan migrate ).
... Tambien puede hechar un vistazo a `config/tables.php` para ver el nombre de las tablas si desea cambiar, no es recomendable ya que estos nombres posee el servidor de "Nevera MYQ"
- Inicializar aplicacion ( `php artisan serve` )
- Inicializar la compilacion scss/typescript ( `npm run watch`), se reiniciara automaticamente por cada cambio que haga en los archivos.
- Si todo ha funcionado, puede dirigirse a [el Link](http://localhost:8000/) y encontrar la pantalla de login.

### Al momento de subir

Para subir los archivos PHP/backend editados/creados, basta con arrastrar a el FTP y remplazar.

En caso de modificar css/js, existen comandos personalizados para compilar los archivos en modo produccion. Ejecute `npm run build:prod` para compilar los archivos js/css y comprimir necesariamente.

Podra subir nuevamente la carpeta public, y los cambios de FrontEnd estaran reflejados.


Este proyecto ha sido creado por medio de [IntuitionStudio](https://www.intuitionstudio.co/) y [Juan Aguillon](https://github.com/juanaguillon) como su respectivo autor.

Saludos.
