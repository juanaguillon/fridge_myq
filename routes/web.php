<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

// Test Routes

use Illuminate\Support\Facades\Mail;

Route::get('/get-behiavor', 'TestsContrller@getDifference');


// Real Routes
Route::get('/', function () {
	if (session()->get('user_id')) {
		return redirect('sell/my-details');
	} else {
		return view('auth.login');
	}
});
Route::get('auth/exit', function () {
	if (session()->get('user_id')) {
		session()->flush();
	}

	return redirect('/');
});


/** Rutas donde es necesario no estar logeado. */
Route::group(array(
	"prefix" => "auth",
	"middleware" => 'auth.nologed'
), function () {

	Route::view("login", 'auth.login');
	Route::view("register", 'auth.register');
	Route::view("restore-password", 'auth.restore-password');

	Route::match(['get', 'post'], "restore-password-tokenized/{userid?}/{usertoken?}", 'Auth\RestorePassword@restorePasswordWithToken');

	Route::post('login', "Auth\LoginController@makeLogin");
	Route::post('register', "Auth\RegisterController@registerUser");
	Route::post('restore-password', "Auth\RestorePassword@restorePassword");
});

// Route::get('difference', 'TestsContrller@differenceTime');

// Rutas que solo el administrador puede acceder
Route::group(array(
	'middleware' => ['auth', 'auth.isadmin']
), function () {

	Route::get("product/edit-products", 'Product\ProductController@editProducts');
	Route::get("product/edit/{id}", 'Product\ProductController@editProduct');
	Route::get("product/get-by-name/{productName}", 'Product\ProductController@getProductByName');
	Route::get('sell/get-historic-sells', 'SellerController@getSaldHistoricSells');
	Route::post('product/create', "Product\ProductController@create");
	Route::post('product/edit', "Product\ProductController@editProductPost");
	Route::view("product/create", 'product.create');


	Route::get('sell/general-details', 'SellerController@getAllSells');
	Route::post('sell/general-details', 'SellerController@ajaxGetAllSells');
	Route::post('sell/disable-sell', 'SellerController@disableSingleSell');
	Route::post('sell/disable-user-sells', 'SellerController@disableSellsByUser');
	Route::post('sell/all-sells-by-date', 'SellerController@getAllSellsByDate');
	Route::post('sell/search-by-name', 'SellerController@getAllSellsByUserName');
	Route::match(['post', 'get'], 'sell/edit-sell/{sellID}', 'SellerController@editSingleSell');

	Route::get('user/get-users', 'UserController@getAllUsers');
	Route::post('user/get-users', 'UserController@getAllUsers');
	Route::post('user/disable-user', 'UserController@disableUser');
	Route::post('user/toggle-user-status', 'UserController@toggleUser');
});

/** Rutas relacionadas con ventas y donde debe estar el usuario logeado. */
Route::group(array(
	"middleware" => 'auth',
), function () {

	Route::get('sell/my-details', 'SellerController@getAllSellsByCurrentUser');
	Route::post("sell/create", "SellerController@createSeller");
	Route::post("sell/sells-by-date ", "SellerController@getSellsByDateAndUser");

	Route::get("product/all", "Product\ProductController@getAllProductsView");
	Route::get("product/all-ajax", "Product\ProductController@getAllProductsAjax");

	Route::get('user/settings', 'UserController@settingsUser');
	Route::post('user/settings', 'UserController@changeSettingsUser');
});
