// /* https://hackernoon.com/lets-start-with-webpack-4-91a0f1dba02e */

const path = require("path");
const extractPluginCss = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const webpack = require('webpack')

const webpackBaseConfig = {
	entry: { app: "./resources/index.webpack.js" },
	output: {
		filename: "[name].js",
		path: path.resolve(__dirname, "./public/assets")
	},
	devServer: {
		compress: true,
		port: 8900,
		inline: true
	},
	module: {
		rules: [
			{
				test: [/.js$|.ts$/],
				exclude: /node_modules/,
				use: [
					{
						loader: "babel-loader",
						options: {
							presets: ["@babel/preset-env", "@babel/typescript"]
						}
					}
				]
			},
			{
				test: [/.css$|.scss$/],
				exclude: /node_modules/,
				use: [
					{
						loader: extractPluginCss.loader,
						options: {
							sourceMap: true
						}
					},
					{
						loader: "css-loader",
						options: {
							sourceMap: true
						}
					},
					{
						loader: "postcss-loader"
					},
					{
						loader: "sass-loader",
						// options: {
						// 	sourceMap: true
						// }
					}
				]
			},
			// {
			// 	test: /\.(ttf|eot|woff|woff2|svg)$/,
			// 	use: {
			// 		loader: "file-loader",
			// 		options: {
			// 			name: "[name].[ext]",
			// 			outputPath: "fonts/",
			// 			publicPath: "/assets/fonts/"
			// 			// path:"./resources/fonts"
			// 		}
			// 	}
			// }
			/* {
        // Use this option only if import images directly in js.
				test: [/\.(png|jpg|svg|gif|jpeg)/],
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[name].[ext]",
							outputPath: "assets/images/"
						}
					}
				]
			} */
		]
	},

	plugins: [
		new extractPluginCss({
			filename: "css/style.css"
		}),
		require("autoprefixer"),
		// new CopyWebpackPlugin([{ from: "./resources/images", to: "./images" }]),
		new CleanWebpackPlugin(),
		new Dotenv({
			path: "./webpack.env"
		}),
		new webpack.SourceMapDevToolPlugin({
			filename: "[file].map"
		})
	],
	resolve: {
		extensions: [".ts", ".js"]
	}
};

const environment = (process.env.NODE_ENV || "development").trim();
if (environment === "development") {
	module.exports = webpackBaseConfig;
} else {
	const merge = require("webpack-merge");
	const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
	const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

	module.exports = merge(webpackBaseConfig, {
		optimization: {
			minimizer: [new UglifyJsPlugin(), new OptimizeCSSAssetsPlugin()]
		}
	});
}
