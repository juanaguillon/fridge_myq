<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use App\Models\SellModel;
use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SellerController extends Controller
{

	/** 
	 * Obtener todas las ventas de los usuarios
	 * @param Int $active Se deben obtener las ventas pendientes? 1 para pendientes, 0 para saldadas.
	 * @return Array
	 */
	private function _getAllSells($active = null)
	{
		$usersTable = config('tables.user');
		$users = DB::select("SELECT user_id, user_name, user_email FROM {$usersTable}");
		$dataUsers = array();
		foreach ($users as $user) {
			$userSell = $this->_getAllSellsByUser($user->user_id, $active);
			if (count($userSell['sells']) == 0) {
				continue;
			}
			$userSell['user_name'] = $user->user_name;
			$userSell['user_email'] = $user->user_email;
			$userSell['user_id'] = $user->user_id;
			$dataUsers[] = $userSell;
		}
		return $dataUsers;
	}

	/**
	 * Obtener todas las ventas en un rango de tiempo.
	 * @param String $from Desde fecha a filtrar
	 * @param String $to Hasta fecha a filtrar
	 * @return App\Models\SellModel
	 */
	private function _getAllSellsByDate($from, $to)
	{
		$productsTable = config('tables.products');
		$sellsTable = config('tables.sells');

		$to = new Carbon($to);

		$sellsQuery = DB::table($sellsTable)
			->join($productsTable, "{$sellsTable}.sell_prod", '=', "{$productsTable}.prod_id")
			->whereBetween('sell_created', [$from, $to->addDay()]);

		return $sellsQuery;
	}




	/**
	 * Obtener todas las ventas de un usuario en concreto en un rango de tiempo.
	 * Retornara un array con llaves "model
	 * @param String $from Desde fecha a filtrar
	 * @param String $to Hasta fecha a filtrar
	 * @param Int $userID Id de usuario a filtrar
	 * @return Array 'model' => App\Models\SellModel, 'total' => TotalDeuda
	 */
	private function _getAllSellsByDateAndUser($from, $to, $userID)
	{
		$usersTable = config('tables.user');
		$sellsTable = config('tables.sells');

		// $sells = $this->_getAllSellsByDate('2019-03-10', '2019-10-07')
		$sells = $this->_getAllSellsByDate($from, $to)
			->join($usersTable, "{$usersTable}.user_id", '=', "{$sellsTable}.sell_user")
			->where('sell_user', $userID);

		$totalSql = "SELECT SUM(sell_quantity * sell_price) as total 
							FROM {$sellsTable} 
							WHERE sell_user = ?
							AND sell_status = 1
							AND sell_created BETWEEN ? AND DATE_ADD(?, INTERVAL 1 DAY) ";
		$total = DB::selectOne($totalSql, array(
			$userID,
			$from,
			$to
		));

		return array(
			"model" => $sells,
			"total" => $total
		);
	}

	/**
	 * Obtener los datos de venta de usuario en especifico.
	 * @param Int $userID ID del usuario a ser procesado y obtener los datos de ventas.
	 * @param Int $active Se deben obtener las ventas pendientes? 1 para pendientes, 0 para saldadas.
	 * @return Array
	 */
	private function _getAllSellsByUser($userID, $active = null)
	{

		$productsTable = config('tables.products');
		$sellsTable = config('tables.sells');

		$sellsQuery = DB::table($sellsTable)
			->join($productsTable, "{$sellsTable}.sell_prod", '=', "{$productsTable}.prod_id")
			->where($sellsTable . '.sell_user', $userID);

		if ($active !== null && is_numeric($active)) {
			$sellsQuery->where("{$sellsTable}.sell_status", $active);
		}


		$query = "SELECT SUM(fs.sell_price * fs.sell_quantity ) as Total 
			FROM {$sellsTable} AS fs 
			INNER JOIN {$productsTable} as fp 
			WHERE fs.sell_user = ? 
			AND fs.sell_status = 1
			AND fs.sell_prod = fp.prod_id";

		$queryResponse = DB::selectOne($query, [$userID]);
		$totalDeuda = $queryResponse->Total;
		$sells = $sellsQuery->get();

		return array(
			'total_deuda' => $totalDeuda,
			'sells' => $sells,
		);
	}

	/**
	 * Obtener todas las ventas con una coincidencia de usuario.
	 * @param array $concidence Debe agregarse un array con todos los "where" necesarios en la sentencia. Cada elemento del array debe tambien ser un array, en el que contiene las claves 'column', 'operator' ( es "=" por defecto ) y el 'search' que sera el valor a coincidir en la columna seleccionada.
	 * @return App\Models\UserModel
	 */
	private function _getAllSellsWithUserConcidence($concidence)
	{

		$users =  new UserModel();

		$usersConcidence = $users->where($concidence)->get();
		// dd($users->where($concidence)->toSql());
		$dataReturned = array();
		foreach ($usersConcidence as $userValue) {
			$sellsUser = $this->_getAllSellsByUser($userValue['user_id'], 1);
			if (count($sellsUser['sells']) == 0) {
				continue;
			}
			$sellsUser['user_email'] = $userValue['user_email'];
			$sellsUser['user_name'] = $userValue['user_name'];
			$sellsUser['user_id'] = $userValue['user_id'];
			$dataReturned[] = $sellsUser;
		}

		return $dataReturned;
	}


	/**
	 * POST sell/create: Crear una nueva venta.
	 */
	public function createSeller(Request $request)
	{
		try {
			$valid = Validator::make($request->all(), array(
				"sell_prod" => "required|numeric",
				"sell_quantity" => "required|min:1|max:20",
				"sell_price" => "required|numeric",
			));


			if ($valid->fails()) {
				return response()->json($valid->errors());
			}

			$currentUserID = $request->session()->get("user_id");
			$sellModel = new SellModel();

			$activeSell = $sellModel->where('sell_user', $currentUserID)
				->where('sell_prod', $request->input("sell_prod"))
				->where("sell_status", 1)
				->first();

			$inserted = false;
			$saved = false;
			// return response()->json($activeSell);
			if ($activeSell) {
				$activeSell->sell_quantity += intval($request->input('sell_quantity'));
				$saved = $activeSell->save();
			} else {
				if (!$currentUserID) {
					$inserted = false;
				} else {
					$inserted = $sellModel::insert(
						array(
							"sell_user" => $currentUserID,
							"sell_prod" => $request->input("sell_prod"),
							"sell_quantity" => $request->input("sell_quantity"),
							"sell_status" => true,
							"sell_price" => $request->input("sell_price"),
							"sell_created" => Carbon::now()->toDateTimeLocalString(),
							"sell_updated" => Carbon::now()->toDateTimeLocalString()
						)
					);
				}
			}

			if ($inserted || $saved) {
				return response()->json(array(
					"newsell" => true,
					"message" => "Venta agregada correctamente"
				));
			} else {
				return response()->json(array(
					"newsell" => false,
					"message" => "Error al agregar nueva venta"
				));
			}
		} catch (\Throwable $th) {
			return response()->json(array(
				"line" => $th->getLine(),
				"code" => $th->getFile(),
				"error" => $th->getMessage(),
			));
		}
	}

	/**
	 * Obtener El historico de saldo en las ventas. Este mostrara datos como total ventidos, total pagado y total deuda con todos los usuarios  
	 * (Aadmin) sell/get-historic-sells
	 */
	public function getSaldHistoricSells()
	{
		$sellsTable = config('tables.sells');
		$userTable = config('tables.user');
		$sql = "SELECT us.user_name, SUM(sl.sell_price * sl.sell_quantity) AS total, 
			SUM(CASE When sl.sell_status = 0 Then sl.sell_price * sl.sell_quantity Else 0 End ) as pagado,
			SUM(CASE When sl.sell_status = 1 Then sl.sell_price * sl.sell_quantity Else 0 End ) as saldo
			FROM {$sellsTable} AS sl
			INNER JOIN {$userTable} AS us ON sl.sell_user = us.user_id
			GROUP BY sl.sell_user
			ORDER BY sl.sell_user ASC;";

		$UsersSerlls = DB::select($sql);
		$allData = array(
			'total' => 0,
			'pagado' => 0,
			'saldo' => 0
		);
		foreach ($UsersSerlls as $usell) {
			$allData['total'] += $usell->total;
			$allData['pagado'] += $usell->pagado;
			$allData['saldo'] += $usell->saldo;
		}

		return view('sell.historic_sells', array(
			'admin_data' => $allData,
			'users_data' => $UsersSerlls
		));
	}


	/**
	 * VISTA sell/my-details: Obtener todas las ventas del actual usuario y mostrar con los datos
	 */
	public function getAllSellsByCurrentUser(Request $request)
	{
		$currentUserID = $request->session()->get('user_id');
		$userData = $this->_getAllSellsByUser($currentUserID);

		return view('sell.my_details', array(
			"sells" => $userData['sells'],
			"total" => $userData['total_deuda']
		));
	}

	/**
	 * Obrener todas las ventas de usuarios con nombres recibidos
	 * (ADMIN) sell/search-by-name
	 * @param Request $request
	 * @return mixed Json Data
	 */
	public function getAllSellsByUserName(Request $request)
	{
		$validateRequest = Validator::make($request->all(), array(
			'user_name' => 'required|string'
		));

		if ($validateRequest->fails()) {
			return response()->json(
				array(
					'error_server' => 'Error Server!'
				)
			);
		}

		$datasTr = $this->_getAllSellsWithUserConcidence(array(
			array(
				'user_name',
				'like',
				"%" . $request->input('user_name') . "%"
			)
		));
		$dataUsers = array();
		foreach ($datasTr as $dtr) {
			$replacedData = array(
				'user_total' => $dtr['total_deuda'],
				'user_sells' => $dtr['sells'],
			);
			$dataUsers[] = array_merge($dtr, $replacedData);
		}


		return response()->json(
			array(
				'view' => view('components.item_sell_by_user_loop', ['allusers' => $dataUsers, 'sald' => true])->render()
			)
		);
	}


	/**
	 * (Admin) VIEW: sell/general-details Obtener todoas las ventas registradas en la plataforma
	 */
	public function getAllSells()
	{

		$allSells = $this->_getAllSells(1);
		return view('sell.all_details', array(
			"all_sells" => $allSells
		));
	}

	/**
	 * Obtener todas las ventas via AJAX.
	 * Este sera utilo en la vista 'all_details', cuando se elimine el filtro por fecha.
	 * (Admin) POST sell/general-details
	 */
	public function ajaxGetAllSells()
	{
		$allSells = $this->_getAllSells(1);
		// dd($allSells);
		$dataUsers = array();
		foreach ($allSells as $sellKey => $sellValue) {
			$replacedData = array(
				'user_total' => $sellValue['total_deuda'],
				'user_sells' => $sellValue['sells'],
			);
			$dataUsers[] = array_merge($sellValue, $replacedData);
		}

		return response()->json(
			array(
				'view' => view('components.item_sell_by_user_loop', ['allusers' => $dataUsers])->render()
			)
		);
	}


	/**
	 * saldar todas las ventas de un usuario en especifico
	 * @param Int $userID ID del usuario a ser saldado
	 * @return boolean True si se han saldado,
	 */
	private function _disableAllSellsByUser($userID)
	{
		$sellModel = SellModel::where('sell_user', $userID)
			->where('sell_status', 1)
			->update(array(
				"sell_status" => 0
			));

		if (!$sellModel) {
			return false;
		}
		return true;
	}

	/**
	 * ( ADMIN ) POST sell/all-sells-by-date Obtener todas las ventas por fecha.
	 */
	public function getAllSellsByDate(Request $request)
	{

		$dataRequest = array(
			'from' => $request->input('date_from'),
			'to' => $request->input('date_to')
		);
		$usersTable = config('tables.user');
		$users = DB::select("SELECT user_id, user_name, user_email FROM {$usersTable}");
		$dataUsers = array();
		foreach ($users as $user) {
			$userData = array();

			$userSellsData = $this->_getAllSellsByDateAndUser($dataRequest['from'], $dataRequest['to'], $user->user_id);
			$userSells = $userSellsData['model']->get();

			if (count($userSells) == 0) continue;

			$userData['user_name'] = $user->user_name;
			$userData['user_total'] = $userSellsData['total']->total;
			$userData['user_email'] = $user->user_email;
			$userData['user_id'] = $user->user_id;
			$userData['user_sells'] = $userSells;
			$dataUsers[] = $userData;
		}

		return response()->json(
			array(
				'view' => view('components.item_sell_by_user_loop', ['allusers' => $dataUsers])->render()
			)
		);
	}

	/**
	 * (Admin) POST: sell/disable-sell Saldar una venta unica
	 *  */
	public function disableSingleSell(Request $request)
	{
		$sellModel = SellModel::find($request->input('sell_id'));
		if ($sellModel) {
			$sellModel->sell_status = false;
			if ($sellModel->save()) {
				return response()->json(
					array(
						'success' => true,
						'code' => 'sell_updated',
						'message' => 'Se ha actualizado la venta correctamente'
					)
				);
			} else {
				return response()->json(
					array(
						'success' => false,
						'code' => 'server_error',
						'message' => 'Error al actualizar la venta.'
					)
				);
			}
		} else {
			return response()->json(
				array(
					'success' => false,
					'code' => 'id_not_found',
					'message' => 'No se ha encontrado el identificador de venta.'
				)
			);
		}
	}

	/**
	 * Obtener las ventas por fecha y de un usuario en especifico.
	 * POST: sell/sells-by-date Obtener las ventas de un usuario en concreto en un rango de tiempo.
	 */
	public function getSellsByDateAndUser(Request $request)
	{

		$dataRequest = array(
			'from' => $request->input('date_from'),
			'to' => $request->input('date_to'),
			'user' => $request->input('date_user')
		);
		if ($dataRequest['user'] != $request->session()->get('user_id')) {
			return response()->json(array('error' => 'Invalid user ID'));
		}

		$sells = $this->_getAllSellsByDateAndUser($dataRequest['from'], $dataRequest['to'], $dataRequest['user']);
		return response()->json(
			array(
				'total' => formatMoney($sells['total']->total),
				'view' => view('components.item_sell_loop', ['sells' => $sells['model']->get()])->render()
			)
		);
	}


	/**
	 * (Admin) POST sell/disable-user-sells Saldar todas las ventas de un usuario en especifico
	 * @
	 */
	public function disableSellsByUser(Request $request)
	{

		$userID = $request->input('sell_user');

		if (!$userID) {
			return response()->json(array(
				'success' => false,
				'code' => 'user_no_send',
				'message' => 'No se ha ingresado ningun usuario'
			));
		}

		$allSellsByuser = $this->_disableAllSellsByUser($userID);
		if ($allSellsByuser) {
			return response()->json(
				array(
					'success' => true,
					'code' => 'disabled_sells',
					'message' => 'Se han saldado las ventas del usuario'
				)
			);
		} else {
			return response()->json(
				array(
					'success' => false,
					'code' => 'server_error',
					'message' => 'Error interno, intente nuevamente.'
				)
			);
		}
	}

	/**
	 * (ADMIN) /sell/edit-sell/{$sellID}
	 * Editar una venta en concreto
	 * @param int $sellID ID De la venta a editar
	 * @return void
	 */
	public function editSingleSell(Request $request, $sellID)
	{

		if (!$sellID) return;

		$sellModel = SellModel::find($sellID);

		if ($request->isMethod('POST')) {
			$valids = Validator::make($request->all(), array(
				'sell_quantity' => 'required',
				'sell_status' => 'required',
			));

			if ($valids->fails()) {
				return response()->json(array(
					'message' => "Error Actualizando venta",
					'success' => false,
				));
			}

			$quantity = $request->input('sell_quantity');
			$status = $request->input('sell_status') == 'true' ? true : false;

			$sellModel->sell_status = $status;
			$sellModel->sell_quantity = $quantity;

			if ($sellModel->save()) {
				return response()->json(array(
					'success' => true,
					'message' => 'Venta actualizada correctamente'
				));
			}
		} else if ($request->isMethod('GET')) {
			$productModel = ProductModel::find($sellModel->sell_prod);
			return view('sell.edit_single_sell', array(
				'sell' => $sellModel,
				'product' => $productModel
			));
		}
	}
}
