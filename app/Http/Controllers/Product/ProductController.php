<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductModel as Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager as Image;



class ProductController extends Controller
{
	public $table = "fridge_products";

	private function getAllProducts($onlyActive = true)
	{
		if ($onlyActive) {
			return Product::where("prod_status", "=", "1")->get();
		} else {
			$product = new Product();
			return $product->all();
		}
	}

	/**
	 * Guardar una imagen de producto. Esta funcion adiciona las proporiciones de la imagen y guardarla en un formato, y tamano estandar.
	 *
	 * @param $image Formato $request->file()
	 * @param string $fileName Nombre de imagen
	 * @return void
	 */
	private function saveProductImage($image, $fileName)
	{
		$imageInter = new Image();
		$imageProced = $imageInter->make($image->getRealPath());
		$width  = $imageProced->width();
		$height = $imageProced->height();

		$dimension = 350;

		$vertical   = $width < $height ? true : false;
		$horizontal = $width > $height ? true : false;

		if ($vertical) {
			// $newWidth = $width + ($height - $width);
			// $imageProced->resize($newWidth, null, function ($constraint) {
			// 	$constraint->aspectRatio();
			// });
			$imageProced->resizeCanvas($height, $height, 'center', false, '#ffffff');
		} else if ($horizontal) {
			$imageProced->resizeCanvas($width, $width, 'center', false, '#ffffff');
			// $newHeight = $height + ($dimension - $height);
			// $imageProced->resize($height, $newHeight, function ($constraint) {
			// 	$constraint->aspectRatio();
			// });
		}
		$imageProced->fit(340);
		$imageProced->save(base_path() . '/public/storage/' . $fileName, 67, 'jpg');
	}

	public function create(Request $request)
	{
		try {
			$validate = Validator::make($request->all(), array(
				"create_product_title"			 => "required|max:250",
				"create_product_price"			 => "required",
				"create_product_off"  			 => "min:0|max:100",
				"create_product_image"  		=> "required"
			));

			if ($validate->fails()) {
				return redirect('product/create')->withErrors($validate->errors())->withInput();
			}

			$image = $request->file("create_product_image");
			$fileName = mt_rand(1000, 9999) .  time() . '.jpg';
			$newProduct = Product::insert(array(
				"prod_nombre" =>	$request->input("create_product_title"),
				"prod_price" =>	$request->input("create_product_price"),
				"prod_off"	=>	$request->input("create_product_off"),
				"prod_desc" => $request->input("create_product_description"),
				"prod_status" => true,
				"prod_image" => $fileName,
				"prod_created" => Carbon::now()->toDateTimeLocalString(),
				"prod_updated" => Carbon::now()->toDateTimeLocalString(),
			));

			if ($newProduct) {
				$this->saveProductImage($image, $fileName);
				return redirect("product/create?newprod=success");
			} else {
				return redirect("product/create?newprod=nonew");
			}
		} catch (\Throwable $th) {
			return response()->json(array(
				"line" => $th->getLine(),
				"code" => $th->getFile(),
				"error" => $th->getMessage(),
			));
		}
	}

	/**
	 * POST product/edit 
	 * Editar un producto.
	 */
	public function editProductPost(Request $request)
	{
		try {
			$prodID = $request->input('create_product_id');
			if (!$prodID) {
				return redirect('product/edit-products');
			}
			$productModel = Product::find($prodID);
			if (!$productModel) {
				return redirect('product/edit-products?newprod=error');
			}

			$validate = Validator::make($request->all(), array(
				"create_product_title"			 => "required|max:250",
				"create_product_price"			 => "required",
				"create_product_off"  			 => "min:0|max:100",
			));

			if ($validate->fails()) {
				return redirect('product/edit-products/' . $prodID . '?newprod=error');
			}

			if (empty($request->input("new_status_prod"))) {
				$newProdStatus = 0;
			} else {
				$newProdStatus = 1;
			}


			$newData = 	array(
				"prod_nombre" =>	$request->input("create_product_title"),
				"prod_price" =>	$request->input("create_product_price"),
				"prod_off"	=>	$request->input("create_product_off"),
				"prod_status" => $newProdStatus,
				"prod_desc" => $request->input("create_product_description"),
				// "prod_status" => true,
			);
			// dd($request->all());

			if ($request->hasFile('create_product_image') && $request->file('create_product_image')->isValid()) {
				$imagePath = base_path() . '/public/storage/' . $productModel->prod_image;
				if (file_exists($imagePath)) {
					unlink($imagePath);
				}
				$image = $request->file("create_product_image");
				$fileName = mt_rand(1000, 9999) .  time() . '.jpg';
				$this->saveProductImage($image, $fileName);
				$productModel->prod_image = $fileName;
			}

			$productModel->prod_nombre = $newData['prod_nombre'];
			$productModel->prod_price = $newData['prod_price'];
			$productModel->prod_off = $newData['prod_off'];
			$productModel->prod_desc = $newData['prod_desc'];
			$productModel->prod_status = $newData['prod_status'];

			if ($productModel->save()) {
				return redirect("product/edit/" . $prodID . "?newprod=success");
			} else {
				return redirect("product/edit/" . $prodID . "?newprod=nonew");
			}
		} catch (\Throwable $th) {
			return response()->json(array(
				"line" => $th->getLine(),
				"code" => $th->getFile(),
				"error" => $th->getMessage(),
			));
		}
	}

	/**
	 * GET product/get-by-name
	 * Obtener todos los productos por nombre
	 * @param string $productName Nombre de Producto a buscar
	 */
	public function getProductByName($productName)
	{
		$products = Product::where('prod_nombre', 'like', "%" . $productName . "%")->where("prod_status","=","1");

		return response()->json(array(
			'view' => view('components.allproducts', array(
				'products' => $products->get()
			))->render(),
		));
	}

	/**
	 * /product/all-ajax
	 * Obtener todos los productos via AJAX
	 * Esto se esta usando para obtener todos los productos en la pagina /product/all
	 */
	public function getAllProductsAjax()
	{
		$allProducts = $this->getAllProducts();

		return response()->json(array(
			'views' => view('components.allproducts', ['products' => $allProducts])->render()
		));
	}


	public function getAllProductsView()
	{
		$allProducts = $this->getAllProducts();
		return view('product.all', array(
			"products" => $allProducts
		));
	}

	public function editProducts()
	{
		$allProducts = $this->getAllProducts(false);
		$urlImages = URL::to('public') . '/storage/';
		return view('product.products_edit', array(
			"products" => $allProducts,
			"urlprod" => $urlImages
		));
	}

	/**
	 * VIEW product/edit/{id} Editar un producto
	 */
	public function editProduct($id)
	{
		$product = Product::find($id);
		if ($product) {
			return view('product.edit', array(
				'product' => $product
			));
		} else {
			return back()->withInput();
		}
	}
}
