<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller{

  /**
   * (Admin) GET user/get-users
   * Obtener todos los usuarios
   */
  public function getAllUsers(){
    $users = UserModel::all();
    return view('user.get_all', array('all_users' => $users ));
  }

  /**
   * Deshabilitar un usuario por ID
   * @param Int $userID Usuario ID
   * @return boolean
   */
  private function _disabelUserById($userID){
    $userModel = UserModel::find( $userID );
    if ( ! $userModel ) return;

    $userModel->user_status = false;
    return $userModel->save();
  }

  /**
   * ( Admin ) POST user/disable-user
   * Deshabilidar usuario por ID. Se usara en la vista 'get-users'
   */
  public function disableUser(Request $request){
    $userIDRequest = $request->input('user_id_to_disable');
    if ( $this->_disabelUserById($userIDRequest)){
      return response()->json(array(
        "success" => true,
        "code" => 'user_disabled_ok',
        "message" => "Se ha deshabilitado el usuario correctamente"
      ));
    }else{
      return response()->json(array(
        "code" => 'user_disabled_err',
        "success" => false,
        "message" => "Ha ocurrido un error mientras se deshabilitado el usuario."
      ));
    }
  }

  /**
   * ( Admin ) POST user/toggle-user-status
   * Deshabilidar usuario por ID. Se usara en la vista 'get-users'
   */
  public function toggleUser(Request $request){
    $userModel = UserModel::find($request->input('user_id_to_toggle'));
    if ( ! $userModel ) return;

    $userModel->user_status = ! $userModel->user_status;
    if ($userModel->save()) {
      return response()->json(array(
        "success" => true,
        "code" => 'user_toggle_ok',
        "message" => "Se ha modificado el estado del usuario correctamente"
      ));
    } else {
      return response()->json(array(
        "code" => 'user_toggle_err',
        "success" => false,
        "message" => "Ha ocurrido un error mientras se modificaba el usuario."
      ));
    }
  }

  /**
   * VIEW user/settings Configuracion de actual usuario
   */
  public function settingsUser(){
    $currentUserID = session()->get('user_id');
    $currentUser = UserModel::find($currentUserID);
    
    return view('user.settings_user', array(
      'current_user' => $currentUser
    ));
  }

  /**
   * POST user/settings Cambiar la configuracion del actual usuario.
   */
  public function changeSettingsUser(Request $request){
    $currentUserID = session()->get('user_id');
    $currentUser = UserModel::find($currentUserID);
    $data = $request->all();

    if ( isset( $data['settings_password']) && $data['settings_cpassword'] !== ''){
      // Verificar si se esta intentando cambiar la contrasena
      if ( !Hash::check($data['settings_cpassword'], $currentUser->user_password )  ){
        return view('user.settings_user', array(
          'current_user' => $currentUser
        ))->with('error', 'Tu contraseña actual no es correcta.');
      }
      $currentUser->user_password = Hash::make($data['settings_newpassword']);
    }

    $currentUser->user_cedula = $data['settings_cedula'];
    $currentUser->user_name = $data['settings_name'];

    if ( $currentUser->save()){
      return view('user.settings_user', array(
        'current_user' => $currentUser
      ))->with('success','Se han guardado los cambios correctamente');
    }else{
      return view('user.settings_user', array(
        'current_user' => $currentUser
      ))->with('error', 'Ha ocurrido un error. Intenta nuevamente');
    }
    
    
  }
  
}
?>