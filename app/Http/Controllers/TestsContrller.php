<?php

/**
 * This file will be only for tests results.
 * You can import this routes in your web.php file to check the api results.
 */

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestsContrller extends Controller
{
    public function getDifference(){
        $data = DB::table('fridge_tests')->where('age', '<>', 22)->get();
        return response()->json(array(
            'dataJson' => $data,
            'sql' => DB::table('fridge_tests')->where('age', '<>', 22)->toSql()
        ));
    }

    public function differenceTime(){

        $now = Carbon::now();
        $last = Carbon::now()->addMinutes(30);
        $result = $now->gte($last);
        return response()->json(array(
            'result' => $result
        ));
    }
}
