<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserModel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
	public function registerUser(Request $request)
	{

		try {
			$newUser = new UserModel();

			$messages = array(
				"required" => "El campo ':attribute' es requerido",
				"required.numeric" => "El numero de cedula es invalido.",
				"required.email" => "Es necesario que proporcione su email",
				"same" => "Las contrasenas no coinciden",
				"register_user_cedula.unique" => "La cédula ya se encuentra registrada.",
				"register_user_email.unique" => 'El email ya se encuentra registrado.'

			);
			$userTable = config('tables.user');
			$validate = Validator::make($request->all(), array(
				"register_user_cedula" => "required|numeric|min:10000|max:99999999999999|unique:{$userTable},user_cedula",
				"register_user_name" => "required",
				"register_user_email" => "required|email|max:250|unique:{$userTable},user_email",
				"register_user_password" => "required|same:register_user_rpassword",
			), $messages);

			if ($validate->fails()) {
				return redirect("auth/register")->withErrors($validate)->withInput();
			}


			$inserted = $newUser::insert(array(
				"user_name" => $request->input("register_user_name"),
				"user_email" => $request->input("register_user_email"),
				"user_status" => true,
				"user_cedula" => $request->input("register_user_cedula"),
				"user_password" => bcrypt($request->input("register_user_password")),
				"user_rol" => 'customer',
				"user_created" => Carbon::now()->toDateTimeLocalString(),
				"user_updated" => Carbon::now()->toDateTimeLocalString()
			));

			if ($inserted) {
				return redirect("auth/register?newreg=success");
			} else {
				return redirect("auth/register?newreg=noreg");
			}
		} catch (\Throwable $th) {
			return response()->json(array(
				"line" => $th->getLine(),
				"code" => $th->getFile(),
				"error" => $th->getMessage(),
			));
		}
	}
}
