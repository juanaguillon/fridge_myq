<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class RestorePassword extends Controller
{
	/**
	 * (NoAuth) POST auth/restore-password
	 */
	public function restorePassword(Request $request)
	{
		$email = $request->input('restore_pw_email');
		$user = UserModel::where('user_email', $email)->first();
		if ($user) {
			$token = bin2hex(random_bytes(48));
			$user->user_token_password = $token;
			$user->user_token_before = Carbon::now()->addMinutes(30);
			$user->user_token_checked = true;
			if ($user->save()) {
				Mail::send('mails.restore_password', array(
					'userID' => $user->user_id,
					'userToken' => $token
				), function ($message) use ($user) {
					$message->to($user->user_email);
					$message->subject('Restaurar Contraseña Nevera MYQ');
					$message->from('myqorg@nevera.com', 'Nevera MYQ');
				});
			}
		}

		return view('auth.restore-password')->with('message', 'Si tu email está registrado, te enviaremos un link con acceso para cambiar tu contraña.');
	}

	/**
	 * (noauth) MATCH[ 'get', 'post' ] auth/restore-password-tokenized Restaurar la contrasena con el token enviado
	 * Esta peticion solo funcionara con el correo enviado en el metodo self::restorePassword
	 * @return view|json Para las peticiones GET
	 */
	public function restorePasswordWithToken(Request $request, $userid = null, $usertoken = null)
	{
		if ($request->isMethod('GET')) {
			// MOSTRAR FORMULARIO PARA CAMBIAR CONTRASENA

			if (!$userid || !$usertoken) return;

			$userID = UserModel::find($userid);

			$canSave = $userID && $userID->user_token_password === $usertoken && Carbon::now()->lessThan($userID->user_token_before) && $userID->user_token_checked;

			if ($canSave) {
				return view('auth.restore_password_form', array(
					'tokenize' => $usertoken,
					'useridentix' => $userid
				));
			} else {
				return response()->json(array(
					'error' => "Data no allowed"
				));
			}

		} else if ($request->isMethod('POST')) {
			// PROCESO PARA CAMBIAR LA CONTRASENA.
			if ($userid !== null || $usertoken !== null) return;
			$userToken = $request->input('restore_pwtoken_token');
			$userID = $request->input('restore_pwtoken_userid');
			$password = $request->input('restore_pwtoken_password');

			$currentUser = UserModel::find($userID);

			$canSave = $currentUser && $currentUser->user_token_password === $userToken && Carbon::now()->lessThan($currentUser->user_token_before) && $currentUser->user_token_checked;

			if ($canSave) {
				// UserTokenChecked servira para revisar si aun esta autorizado para cambiar la contrasena. No se podra cambiar dos veces con el mismo email. Debera solicitar nuevamente la restauracion de contrasena.
				$currentUser->user_token_checked = false;
				$currentUser->user_password = bcrypt($password);

				if ($currentUser->save()) {
					return view('auth.restore_password_form', array(
						'tokenize' => $userToken,
						'useridentix' => $userID
					))->with('success', 'Contraseña actualizada correctamente');
				} else {
					return view('auth.restore_password_form', array(
						'tokenize' => $userToken,
						'useridentix' => $userID
					))->with('error', 'Error interno al actualizar contraseña, intente nuevamente');
				}
			} else {
				return view('auth.restore_password_form', array(
					'tokenize' => $userToken,
					'useridentix' => $userID
				))->with('error', 'Datos Inválidos');
			}
		}
	}
}
