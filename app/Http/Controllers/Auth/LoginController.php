<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserModel;
use Illuminate\Support\Facades\Hash; 

class LoginController extends Controller{

  public function makeLogin(Request $request){
    try {
      $existsUser = new UserModel();
      $user = $existsUser::where("user_email", $request->input("login_form_email"))->first();

      
      if ( Hash::check($request->input("login_form_password"), $user['user_password'])){
        $request->session()->put("user_id", $user['user_id']);
        $request->session()->put("user_rol", $user['user_rol']);
        $request->session()->put("user_email", $user['user_email']);
        $request->session()->put("user_name", $user['user_name']);
        return redirect('sell/my-details');
      }else{
        return redirect('auth/login?login=errlog');
      }
      
    } catch (\Throwable $th) {
      return response()->json(array(
        "line" => $th->getLine(),
        "code" => $th->getFile(),
        "error" => $th->getMessage(),
      ));
    }
    

  }
}