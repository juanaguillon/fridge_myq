<?php

namespace App\Http\Middleware;

use Closure;

class checkIfIsAdmin
{

  public function handle($request, Closure $next)
  {
    if ($request->session()->get('user_rol') !== 'admin') {
      return redirect('/');
    }
    return $next($request);
  }
}
