<?php

use Intervention\Image\ImageManager as Image;

/**
 * Mostrar formato a un nombre de usuario
 *
 * @param string $userName
 * @return string Username formateado.
 */
function formatUserName($userName)
{
  return ucwords(strtolower($userName));
}

/**
 * Mostrar formato de moneda local
 *
 * @param int $numberMoney
 * @return string Moneda formateada
 */
function formatMoney($numberMoney)
{
  $numberMoney = intval($numberMoney);
  return "$" . number_format($numberMoney, 0, '.', '.');
}
