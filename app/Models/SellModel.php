<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellModel extends Model
{
    //
    public $primaryKey = "sell_id";

    public $table = '';

    const CREATED_AT = 'sell_created';
    const UPDATED_AT = 'sell_updated';

    public function __construct()
    {
        $this->table = config('tables.sells');
    }
}
