<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    public $primaryKey = 'user_id';

    protected $hidden = ["user_password"];

    const CREATED_AT = 'user_created';
    const UPDATED_AT = 'user_updated';

    public $table = '';

    public function __construct()
    {
        $this->table = config('tables.user');
    }
}
