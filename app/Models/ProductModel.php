<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    public $primaryKey = "prod_id";

    public $table = '';

    const CREATED_AT = 'prod_created';
    const UPDATED_AT = 'prod_updated';

    public function __construct()
    {
        $this->table = config('tables.products');
    }
}
